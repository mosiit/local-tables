USE [impresario]
GO

/****** Object:  Table [dbo].[LT_M2_WELCOME_SENT_LOG]    Script Date: 7/9/2019 12:11:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_M2_WELCOME_SENT_LOG](
	[row_id] [INT] IDENTITY(1,1) NOT NULL,
	[affiliation_type] [VARCHAR](2) NOT NULL,
	[order__member_id] [INT] NULL,
	[customer_no] [INT] NULL,
	[memb_level] [VARCHAR](3) NULL,
	[email_address] [VARCHAR](80) NOT NULL,
	[email_primary_ind] [CHAR](1) NOT NULL,
	[email_inactive] [CHAR](1) NOT NULL,
	[custom_date] [DATE] NULL,
	[init_dt] [DATE] NULL,
	[order__NRR_Status] [VARCHAR](12) NOT NULL,
	[current_status_name] [VARCHAR](30) NULL,
	[member_name] [VARCHAR](160) NULL,
	[order__one_step] [VARCHAR](1) NOT NULL,
	[inactive] [CHAR](1) NULL,
	[created_by] [VARCHAR](8) NULL,
	[created_dt] [DATETIME] NULL,
	[created_loc] [VARCHAR](16) NULL,
	[last_updated_by] [VARCHAR](8) NULL,
	[last_updated_dt] [DATETIME] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_inactive]  DEFAULT ('N') FOR [inactive]
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_create_dt]  DEFAULT (GETDATE()) FOR [created_dt]
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_create_loc]  DEFAULT (HOST_NAME()) FOR [created_loc]
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
GO

ALTER TABLE [dbo].[LT_M2_WELCOME_SENT_LOG] ADD  CONSTRAINT [DF_LT_M2_WELCOME_SENT_LOG_last_update_dt]  DEFAULT (GETDATE()) FOR [last_updated_dt]
GO


