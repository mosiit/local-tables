USE [impresario]
GO

    SET ANSI_NULLS ON
    SET ANSI_PADDING ON
    SET ANSI_WARNINGS ON
GO

    --DROP TABLE [dbo].[LTR_OVERNIGHT_SCHEDULE]
GO

    CREATE TABLE [dbo].[LTR_OVERNIGHT_SCHEDULE]  ([schedule_id] INT IDENTITY(1,1) NOT NULL,
                                                  [schedule_dt] DATE NULL,
                                                  [schedule_staff] INT NOT NULL DEFAULT (0),
                                                  [first_activity] INT NOT NULL DEFAULT (0),
                                                  [second_activity] INT NOT NULL DEFAULT (0),
                                                  [notes] VARCHAR(255) NOT NULL DEFAULT(''),
                                                  [create_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
                                                  [created_by] VARCHAR(10) NOT NULL DEFAULT([dbo].[fs_user]()),
                                                  [create_loc] VARCHAR(25) NOT NULL DEFAULT(HOST_NAME()),
                                                  [last_update_dt] [datetime] NULL DEFAULT (GETDATE()),
                                                  [last_updated_by] [varchar] (10) NULL DEFAULT ([dbo].[fs_user]()),
                                                  [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                                  [control_group]  INT NOT NULL DEFAULT(8),
    CONSTRAINT [PK_LTR_OVERNIGHT_SCHEDULE] PRIMARY KEY NONCLUSTERED 
              ([schedule_id] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])  ON [PRIMARY]

GO

    GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_OVERNIGHT_SCHEDULE] TO ImpUsers
GO

    IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERNIGHT_ACTIVITIES]') AND name = N'ix_LTR_OVERNIGHT_ACTIVITIES_activity_dt_activity_name')
        CREATE UNIQUE CLUSTERED INDEX [ix_LTR_OVERNIGHT_schedule_dt_schedule_staff] ON [dbo].[LTR_OVERNIGHT_SCHEDULE] ([schedule_dt] ASC, [schedule_staff])
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

    EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_OVERNIGHT_SCHEDULE'
GO

    UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LV_MOS_RESOURCES_DROPDOWN', [dddw_value] = 'id', [dddw_description] = 'resource_name', [dddw_where] = 'resource_type = ''Instructor Staff'' and inactive = ''N'''
    WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_OVERNIGHT_SCHEDULE') and [column_name] = 'schedule_staff'
GO

    UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LV_MOS_RESOURCES_DROPDOWN', [dddw_value] = 'id', [dddw_description] = 'resource_name', [dddw_where] = 'resource_type IN ('''', ''Drop-In Activity'') and inactive = ''N'''
    WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_OVERNIGHT_SCHEDULE') and [column_name] = 'first_activity'
GO

    UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LV_MOS_RESOURCES_DROPDOWN', [dddw_value] = 'id', [dddw_description] = 'resource_name', [dddw_where] = 'resource_type IN ('''', ''DROP-In Activity'') and inactive = ''N'''
    WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_OVERNIGHT_SCHEDULE') and [column_name] = 'second_activity'
GO

    IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERNIGHT_SCHEDULE_TU]'))
        DROP TRIGGER [dbo].[LTR_OVERNIGHT_SCHEDULE_TU]
GO

    CREATE TRIGGER [dbo].[LTR_OVERNIGHT_SCHEDULE_TU] ON [dbo].[LTR_OVERNIGHT_SCHEDULE] FOR UPDATE
    AS BEGIN

        SET NOCOUNT ON 

        --Update the last_updated_by, last_update_dt columns
        IF NOT (UPDATE(last_update_dt)) AND NOT (UPDATE(last_updated_by))
            UPDATE 	[dbo].[LTR_OVERNIGHT_SCHEDULE]
            SET 	[last_updated_by] = dbo.FS_USER(), [last_update_dt] = getdate()
            FROM	[dbo].[LTR_OVERNIGHT_SCHEDULE] a, inserted b
            WHERE	a.schedule_id = b.schedule_id
 
    END

GO


SELECT * FROM [dbo].[LTR_OVERNIGHT_SCHEDULE]
