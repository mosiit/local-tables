USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO


CREATE TABLE LT_TESSITURA_COPY_DOWN_HISTORY
(
	id INT IDENTITY(1,1), 
	Environment VARCHAR(50) NOT NULL, -- Test, QA, Stage
	SQLServerName VARCHAR(50) NOT NULL,
	TessituraVersion VARCHAR(50) NOT NULL,
	CopyDownDate DATETIME NOT NULL,
	BackupDate DATETIME NOT NULL, 
	WhoDidCopyDown VARCHAR(50) NOT NULL, 
	Notes VARCHAR(200),
 CONSTRAINT [PK_LT_TESSITURA_COPY_DOWN_HISTORY] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET ANSI_PADDING OFF
GO

-- Insert statement for Copy Down document
--INSERT INTO dbo.LT_TESSITURA_COPY_DOWN_HISTORY
--(
--	Environment,
--	SQLServerName,
--	TessituraVersion,
--	CopyDownDate,
--	BackupDate,
--	WhoDidCopyDown,
--	Notes
--)
--SELECT 
--	'Test', 
--	'Wolverine',
--	dbo.FS_CURRENT_VERSION(), 
--	GETDATE(), 
--	'7/24/2018 5:14AM',
--	'Harry Potter', -- replace with your name
--	'Copy down requested by Annie because...? ' -- please fill in the Notes. If it is for a SP or Enhanced Entitlements or etc. 
