USE [impresario]
GO

/****** Object:  Table [dbo].[LTR_ENTITLEMENT]    Script Date: 4/12/2018 10:30:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LTR_ENTITLEMENT](
	[entitlement_no] [INT] IDENTITY(1,1) NOT NULL,
	[entitlement_code] [CHAR](10) NULL,
	[entitlement_desc] [VARCHAR](255) NULL,
	[memb_level_no] [INT] NOT NULL,
	[ent_tkw_id] [INT] NOT NULL,
	[ent_price_type_group] [INT] NOT NULL,
	[num_ent] [INT] NULL,
	[reset_type] [CHAR](1) NOT NULL,
	[decline_benefit] [CHAR](1) NULL CONSTRAINT [DF_LTR_ENT_decline_benefit]  DEFAULT ('Y'),
	[allow_lapsed] [CHAR](1) NULL,
	[date_to_compare] [CHAR](1) NULL,
	[custom_1] [VARCHAR](255) NULL,
	[custom_2] [VARCHAR](255) NULL,
	[custom_3] [VARCHAR](255) NULL,
	[custom_4] [VARCHAR](255) NULL,
	[custom_5] [VARCHAR](255) NULL,
	[custom_6] [VARCHAR](255) NULL,
	[custom_7] [VARCHAR](255) NULL,
	[custom_8] [VARCHAR](255) NULL,
	[custom_9] [VARCHAR](255) NULL,
	[custom_10] [VARCHAR](255) NULL,
	[inactive] [CHAR](1) NULL CONSTRAINT [DF_LTR_ENT_inactive]  DEFAULT ('N'),
	[created_by] [CHAR](8) NULL CONSTRAINT [DF_LTR_ENT_created_by]  DEFAULT (USER_NAME()),
	[create_dt] [DATETIME] NULL CONSTRAINT [DF_LTR_ENT_create_dt]  DEFAULT (GETDATE()),
	[last_updated_by] [CHAR](8) NULL,
	[last_update_dt] [DATETIME] NULL,
PRIMARY KEY CLUSTERED 
(
	[entitlement_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_ENT] UNIQUE NONCLUSTERED 
(
	[memb_level_no] ASC,
	[ent_tkw_id] ASC,
	[ent_price_type_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LTR_ENTITLEMENT]  WITH CHECK ADD FOREIGN KEY([memb_level_no])
REFERENCES [dbo].[T_MEMB_LEVEL] ([memb_level_no])
GO


