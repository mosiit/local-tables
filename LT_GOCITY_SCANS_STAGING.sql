USE [impresario]
GO

/****** Object:  Table [dbo].[LT_GOCITY_SCANS_STAGING]    Script Date: 1/9/2020 1:18:30 PM ******/
DROP TABLE [dbo].[LT_GOCITY_SCANS_STAGING]
GO

/****** Object:  Table [dbo].[LT_GOCITY_SCANS_STAGING]    Script Date: 1/9/2020 1:18:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_GOCITY_SCANS_STAGING](
	[go_id] [INT] DEFAULT 0,
	[file_name] [NVARCHAR](100) NULL,
	[go_scan_dt] [VARCHAR](50) NULL,
	[go_confirmation_code] [VARCHAR](50) NULL,
	[go_pass_type] [VARCHAR](50) NULL,
	[go_product_type] [VARCHAR](50) NULL,
	[go_attraction] [VARCHAR](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


