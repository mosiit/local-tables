USE [impresario]
GO

/****** Object:  Table [dbo].[LT_LOCAL_TABLES_CUSTOMER_NO]    Script Date: 1/4/2017 10:55:35 AM ******/
DROP TABLE [dbo].[LT_LOCAL_TABLES_CUSTOMER_NO]
GO

/****** Object:  Table [dbo].[LT_LOCAL_TABLES_CUSTOMER_NO]    Script Date: 1/4/2017 10:55:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_LOCAL_TABLES_CUSTOMER_NO](
	[table_name] [varchar](300) NOT NULL,
	[column_name] [varchar](300) NOT NULL,
	[created_by] [varchar](8) NULL,
	[created_dt] [datetime] NULL,
	[created_loc] [varchar](16) NULL,
	[last_updated_by] [varchar](8) NULL,
	[last_updated_dt] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


