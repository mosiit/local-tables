

CREATE TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ([log_no] INT NOT NULL IDENTITY (1,1), 
                                                   [course_year] CHAR(4) NOT NULL, 
                                                   [order_no] int NOT NULL, 
                                                   [print_dt] DATETIME NOT NULL,
                                                   [is_reprint] CHAR(1) NOT NULL,
                                                   [created_by] [varchar](8) NULL,
	                                               [create_dt] [datetime] NULL,
    	                                           [create_loc] [varchar](16) NULL,
	                                               [last_updated_by] [varchar](8) NULL,
    	                                           [last_update_dt] [datetime] NULL)
GO

GRANT SELECT, INSERT, UPDATE ON [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] TO impusers
GO

CREATE CLUSTERED INDEX [ix_LT_COURSE_ADVANCE_LETTER_LOG_course_year] ON [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ([course_year] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_COURSE_ADVANCE_LETTER_LOG_order_no] ON [dbo].[LT_HISTORY_TICKET] ([order_no] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_course_year]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_course_year]  DEFAULT (DATENAME(YEAR,GETDATE())) FOR [course_year]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_order_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_order_no]  DEFAULT (0) FOR [order_no]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_print_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_print_dt]  DEFAULT (GETDATE()) FOR [print_dt]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_is_reprint]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_is_reprint]  DEFAULT ('N') FOR [is_reprint]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LT_COURSE_ADVANCE_LETTER_LOG_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ADD  CONSTRAINT [DF_LT_COURSE_ADVANCE_LETTER_LOG_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO
