USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERALL_CAMPAIGN]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_OVERALL_CAMPAIGN](
	    [id] [int] NOT NULL,
    	[description] [varchar](30) NULL,
	    [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_OVERALL_CAMPAIGN] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_description]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_description]  DEFAULT ('') FOR [description]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_OVERALL_CAMPAIGN_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_OVERALL_CAMPAIGN] ADD  CONSTRAINT [DF_LTR_OVERALL_CAMPAIGN_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_OVERALL_CAMPAIGN] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_OVERALL_CAMPAIGN'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERALL_CAMPAIGN_TU]'))
    DROP TRIGGER [dbo].[LTR_OVERALL_CAMPAIGN_TU]
GO

CREATE TRIGGER [dbo].[LTR_OVERALL_CAMPAIGN_TU] ON [dbo].[LTR_OVERALL_CAMPAIGN] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_OVERALL_CAMPAIGN]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_OVERALL_CAMPAIGN] a, inserted b
    WHERE	a.id = b.id
 
END
GO