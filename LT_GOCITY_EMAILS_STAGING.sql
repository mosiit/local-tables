USE [impresario]
GO

/****** Object:  Table [dbo].[LT_GOCITY_EMAILS_STAGING]    Script Date: 1/8/2020 1:58:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[LT_GOCITY_EMAILS_STAGING]
GO

CREATE TABLE [dbo].[LT_GOCITY_EMAILS_STAGING](
	[Body] NVARCHAR(MAX) NULL,
	[BodyHTML] NVARCHAR(MAX) NULL,
	[Subject] VARCHAR(MAX) NULL,
	[FromEmail] VARCHAR(MAX) NULL,
	[DateSent] DATETIME NULL,
	[To] VARCHAR(MAX) NULL,
	[Size] INT NULL,
	[Priority] INT NULL,
	[Attachments] VARCHAR(MAX) NULL,
	[RelatedItems] VARCHAR(MAX) NULL,
	[MessageID] VARCHAR(MAX) NULL,
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


