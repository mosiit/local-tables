USE [impresario]
GO

/****** Object:  Trigger [dbo].[LTX_CUST_ENT_TI]    Script Date: 3/16/2020 2:58:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tessitura Network Jon Ballinger
-- Create date: 2/9/2018
-- Description:	Enforces new note when customer entitlement is added from custom screen.  Writes entry to audit table.
-- AAD-B Edited 11/7/2019 for TS Ticket #76820 so inserted relative date entitlements default to expire at 23:59
-- JTS edited 2/24/2020 for TS #76820 revisions to limit relative-date updates to just the inserted record 
-- =============================================

ALTER TRIGGER [dbo].[LTX_CUST_ENT_TI]
ON [dbo].[LTX_CUST_ENTITLEMENT]
FOR INSERT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DECLARE @value VARCHAR(500);
    SELECT @value = dbo.FS_GET_PARAM_FROM_APPNAME('CT');


    DECLARE @from_tg VARCHAR(500);
    SELECT @from_tg = COALESCE(dbo.FS_GET_PARAM_FROM_APPNAME('gift_tg'), 'N');


    IF EXISTS
    (
        SELECT 1
        FROM inserted
        WHERE (
                  note IS NULL
                  OR LEN(note) < 1
              )
              AND @value IS NOT NULL
              AND COALESCE(@from_tg, 'N') = 'N'
    )
    BEGIN
        ROLLBACK TRANSACTION;
        RAISERROR('Please enter a note. ', 16, 1);
        RETURN;

    END;


    IF EXISTS
    (
        SELECT 1
        FROM inserted
        WHERE COALESCE(entitlement_no, 0) < 1
              AND @value IS NOT NULL
    )
    BEGIN
        ROLLBACK TRANSACTION;
        RAISERROR('Please select an entitlement.', 16, 1);
        RETURN;
    END;

    EXEC AP_SET_CONTEXT 'entitle_tg', 'Y';

    --11/5/2019 --works for relative number of days but not fixed date
    UPDATE a
    SET a.init_dt = COALESCE(c.fixed_init_dt, GETDATE()),
        a.expr_dt = COALESCE(
                                DATEADD(MINUTE, -1, DATEDIFF(DAY, 0, c.fixed_expr_dt + 1)),
                                DATEADD(
                                           DAY,
                                           c.relative_num_days,
                                           (CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 112) + ' 23:59:00.000'))
                                       )
                            ),
        a.num_ent = COALESCE(a.num_ent, c.num_ent)
    FROM dbo.LTX_CUST_ENTITLEMENT a
        JOIN inserted b
            ON a.id_key = b.id_key
        JOIN LTR_ENTITLEMENT c
            ON a.entitlement_no = c.entitlement_no
               AND c.is_adhoc = 'Y'
    WHERE b.expr_dt IS NULL
          OR b.init_dt IS NULL;

    INSERT INTO LTX_AUDIT_CUST_ENTITLEMENT
    (
        [id_key],
        [customer_no],
        [cust_memb_no],
        [entitlement_no],
        [entitlement_date],
        [entitlement_perf_no],
        [num_ent],
        [created_by],
        [create_dt],
        [last_updated_by],
        [last_update_dt],
        [set_no],
        [init_dt],
        [expr_dt],
        [inactive],
        [note],
        [gift_no],
        action
    )
    SELECT [id_key],
           [customer_no],
           [cust_memb_no],
           [entitlement_no],
           [entitlement_date],
           [entitlement_perf_no],
           [num_ent],
           [created_by],
           [create_dt],
           [last_updated_by],
           [last_update_dt],
           [set_no],
           [init_dt],
           [expr_dt],
           [inactive],
           [note],
           [gift_no],
           'Insert'
    FROM inserted;

    -- 2nd update added 11/7/2019 to deal with the fixed expiration dates that need to be overridden but still keep the 11:59 end time.
	-- updated 2/24/2020 J Smillie added join to inserted to limit update to just the new record to avoid mass updates 
    UPDATE a
    SET a.expr_dt = DATEADD(MINUTE, -1, DATEDIFF(DAY, 0, a.expr_dt + 1))
    FROM dbo.LTX_CUST_ENTITLEMENT a JOIN inserted b -- this was added 
	ON a.id_key = b.id_key -- so was this 
    WHERE a.expr_dt IS NOT NULL;


    EXEC AP_SET_CONTEXT 'entitle_tg', 'N';
END;



GO


