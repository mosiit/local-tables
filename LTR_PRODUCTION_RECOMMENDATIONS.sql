USE [impresario]
GO

/****** Object:  Table [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]    Script Date: 1/21/2017 12:38:38 PM ******/

DROP TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]
GO

/****** Object:  Table [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]    Script Date: 1/21/2017 12:38:38 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS](
	[id] [INT] NOT NULL,
	[production] [INT] NOT NULL,
	[rec_weight] INT NOT NULL,
    [start_dt] [DATETIME] NULL,
	[end_dt] [DATETIME] NULL,
	[rec_type] [VARCHAR](30) NOT NULL,
    [rec_prod_01] [INT] NOT NULL,
	[rec_prod_02] [INT] NOT NULL,
	[rec_prod_03] [INT] NOT NULL,
	[rec_prod_04] [INT] NOT NULL,
	[rec_prod_05] [INT] NOT NULL,
	[rec_prod_06] [INT] NOT NULL,
	[rec_prod_07] [INT] NOT NULL,
	[rec_prod_08] [INT] NOT NULL,
	[rec_prod_09] [INT] NOT NULL,
	[rec_prod_10] [INT] NOT NULL,
    [inactive] [CHAR](1) NOT NULL,
    [created_by] [varchar](8) NOT NULL,
    [create_dt] [datetime] NOT NULL,
    [create_loc] [varchar](16) NOT NULL,
	[last_updated_by] [varchar](8) NOT NULL,
    [last_update_dt] [datetime] NOT NULL,
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_PADDING ON
GO

CREATE UNIQUE CLUSTERED INDEX [uq_ltr_production_recommendations_production_rec_type] ON [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]
                              ([production] ASC, [rec_type] ASC)
       WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_id]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_id]  DEFAULT ((0)) FOR [id]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_production]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_production]  DEFAULT ((0)) FOR [production]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_type]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_type]  DEFAULT ('') FOR [rec_type]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_weight]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_weight]  DEFAULT ((0)) FOR [rec_weight]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_01]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_01]  DEFAULT ((0)) FOR [rec_prod_01]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_02]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUmsherw00    CTION_RECOMMENDATIONS_rec_prod_02]  DEFAULT ((0)) FOR [rec_prod_02]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_03]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_03]  DEFAULT ((0)) FOR [rec_prod_03]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_04]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_04]  DEFAULT ((0)) FOR [rec_prod_04]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_05]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_05]  DEFAULT ((0)) FOR [rec_prod_05]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_06]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_06]  DEFAULT ((0)) FOR [rec_prod_06]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_07]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_07]  DEFAULT ((0)) FOR [rec_prod_07]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_08]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_08]  DEFAULT ((0)) FOR [rec_prod_08]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_09]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_09]  DEFAULT ((0)) FOR [rec_prod_09]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_10]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_rec_prod_10]  DEFAULT ((0)) FOR [rec_prod_10]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_inactive]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_inactive]  DEFAULT ('N') FOR [inactive]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_created_by]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_create_dt]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_create_dt]  DEFAULT (getdate()) FOR [create_dt]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_create_loc]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_create_loc]  DEFAULT (host_name()) FOR [create_loc]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_last_updated_by]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_PRODUCTION_RECOMMENDATIONS_last_update_dt]') AND type = 'D')
    ALTER TABLE [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] ADD  CONSTRAINT [DF_LTR_PRODUCTION_RECOMMENDATIONS_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
GO


GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] TO ImpUsers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_PRODUCTION_RECOMMENDATIONS'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [dddw_table] = 'TR_GOOESOFT_DROPDOWN', [dddw_value] = 'description', [dddw_description] = 'description', [dddw_where] = 'code = 1019 and inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PRODUCTION_RECOMMENDATIONS') and [column_name] = 'rec_type'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [dddw_table] = 'LV_RECOMMENDED_PRODUCTIONS', [dddw_value] = 'production_no', [dddw_description] = 'production_name', [dddw_where] = ''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PRODUCTION_RECOMMENDATIONS') and [column_name] like 'rec_prod%'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [dddw_table] = 'LV_RECOMMENDATION_PRODUCTIONS', [dddw_value] = 'production_no', [dddw_description] = 'production_name', [dddw_where] = ''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PRODUCTION_RECOMMENDATIONS') and [column_name] = 'production'
GO


IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_PRODUCTION_RECOMMENDATIONS_TU]'))
    DROP TRIGGER [dbo].[LTR_PRODUCTION_RECOMMENDATIONS_TU]
GO

CREATE TRIGGER [dbo].[LTR_PRODUCTION_RECOMMENDATIONS_TU] ON [dbo].[LTR_PRODUCTION_RECOMMENDATIONS] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_PRODUCTION_RECOMMENDATIONS]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_PRODUCTION_RECOMMENDATIONS] a, inserted b
    WHERE	a.id = b.id
 
END
GO


--SELECT * FROM dbo.TR_GOOESOFT_DROPDOWN WHERE CODE = 1019
--SELECT * FROM [dbo].[LTR_PRODUCTION_RECOMMENDATIONS]