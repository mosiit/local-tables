USE [impresario]
GO

/****** Object:  Table [dbo].[LT_DS_VENUES]    Script Date: 3/17/2020 11:22:16 AM ******/
DROP TABLE [dbo].[LT_DS_VENUES]
GO

/****** Object:  Table [dbo].[LT_DS_VENUES]    Script Date: 3/17/2020 11:22:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LT_DS_VENUES](
	[id] [INT] NOT NULL,
	[venue_name] [NVARCHAR](300) NULL
) ON [PRIMARY]

GO


