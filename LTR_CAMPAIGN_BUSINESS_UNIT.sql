USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT](
	[id] [INT] NOT NULL IDENTITY(1,1),
	[campaign_no] INT NOT NULL,
	[Business_Unit] VARCHAR(500) NOT NULL,
	[Unrestricted_Goal] DECIMAL(10,2), 
	[Restricted_Goal] DECIMAL(10,2), 
	[inactive] CHAR(1) NOT NULL, 
	[created_by] [VARCHAR](8) NULL,
	[create_dt] [DATETIME] NULL,
	[create_loc] [VARCHAR](16) NULL,
	[last_updated_by] [VARCHAR](8) NULL,
	[last_update_dt] [DATETIME] NULL,
 CONSTRAINT [PK_LTR_CAMPAIGN_BUSINESS_UNIT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [LTR_CAMPAIGN_BUSINESS_UNIT] ADD CONSTRAINT UC_LTR_CAMPAIGN_BUSINESS_UNIT_campaign_no UNIQUE (campaign_no)
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_inactive]  DEFAULT ('N') FOR [inactive]
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_create_dt]  DEFAULT (GETDATE()) FOR [create_dt]
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_create_loc]  DEFAULT (HOST_NAME()) FOR [create_loc]
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
GO

ALTER TABLE [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] ADD  CONSTRAINT [DF_LTR_CAMPAIGN_BUSINESS_UNIT_last_update_dt]  DEFAULT (GETDATE()) FOR [last_update_dt]
GO

---- Run this Tessitura Stored Proc to make this table a SYSTEM TABLE.
---- Only needs to be run once.
--EXEC dbo.UP_POPULATE_REFERENCE_METADATA @table_name = 'LTR_CAMPAIGN_BUSINESS_UNIT'

GRANT SELECT,UPDATE,INSERT,DELETE ON dbo.LTR_CAMPAIGN_BUSINESS_UNIT TO ImpUsers; 