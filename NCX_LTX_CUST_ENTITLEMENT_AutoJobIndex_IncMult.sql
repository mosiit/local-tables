USE [impresario]
GO

/****** Object:  Index [NCX_LTX_CUST_ENTITLEMENT_AutoJobIndex_IncMult]    Script Date: 11/22/2019 11:18:08 AM ******/
DROP INDEX [NCX_LTX_CUST_ENTITLEMENT_AutoJobIndex_IncMult] ON [dbo].[LTX_CUST_ENTITLEMENT]
GO

/****** Object:  Index [NCX_LTX_CUST_ENTITLEMENT_AutoJobIndex_IncMult]    Script Date: 11/22/2019 11:18:08 AM ******/
CREATE NONCLUSTERED INDEX [NCX_LTX_CUST_ENTITLEMENT_AutoJobIndex_IncMult] ON [dbo].[LTX_CUST_ENTITLEMENT]
(
	[entitlement_no] ASC
)
INCLUDE ( 	[id_key],
	[cust_memb_no],
	[entitlement_date],
	[num_ent]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


