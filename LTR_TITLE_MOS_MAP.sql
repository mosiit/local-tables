USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_TITLE_MOS_MAP]') AND type in (N'U'))
--    DROP TABLE [dbo].[LTR_TITLE_MOS_MAP]
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_TITLE_MOS_MAP]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_TITLE_MOS_MAP](
	    [id] [int] NOT NULL,
        [tessitura_title] [int] NOT NULL,
        [mode_of_sale] [int] NOT NULL,
        [performance_type] [int] NOT NULL,
        [date_before] [char] (10) NOT NULL,
        [days_before] int NOT NULL,
        [minutes_before] int NOT NULL,
        [date_after] [char] (10) NOT NULL,
        [days_after] int NOT NULL,
        [minutes_after] int NOT NULL,
        [notes] [varchar] (255) NULL,
        [inactive] [char](1) NOT NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_TITLE_MOS_MAP] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'uq_LTR_TITLE_MOS_MAP_tessitura_title_mode_of_sale_performance_type')
    DROP INDEX [uq_LTR_TITLE_MOS_MAP_tessitura_title_mode_of_sale_performance_type] ON [dbo].[LTR_TITLE_MOS_MAP]
GO

CREATE UNIQUE NONCLUSTERED INDEX [uq_LTR_TITLE_MOS_MAP_tessitura_title_mode_of_sale_performance_type] ON [dbo].[LTR_TITLE_MOS_MAP] ([tessitura_title] ASC,[mode_of_sale] ASC, [performance_type] ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_tessitura_title]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_tessitura_title]  DEFAULT (0) FOR [tessitura_title]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_mode_of_sale]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_mode_of_sale]  DEFAULT (0) FOR [mode_of_sale]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_date_before]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_date_before]  DEFAULT ('') FOR [date_before]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_days_before]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_days_before]  DEFAULT (0) FOR [days_before]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_minutes_before]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_minutes_before]  DEFAULT (0) FOR [minutes_before]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_date_after]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_date_after]  DEFAULT ('') FOR [date_after]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_days_after]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_days_after]  DEFAULT (0) FOR [days_after]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_minutes_after]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_minutes_after]  DEFAULT (0) FOR [minutes_after]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_notes]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_notes]  DEFAULT ('') FOR [notes]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_TITLE_MOS_MAP_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_TITLE_MOS_MAP] ADD  CONSTRAINT [DF_LTR_TITLE_MOS_MAP_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_TITLE_MOS_MAP] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_TITLE_MOS_MAP'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'TR_MOS', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_TITLE_MOS_MAP') and [column_name] = 'mode_of_sale'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'T_INVENTORY', [dddw_value] = 'inv_no', [dddw_description] = 'description', [dddw_where] = 'type = ''T'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_TITLE_MOS_MAP') and [column_name] = 'tessitura_title'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'TR_PERF_TYPE', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_TITLE_MOS_MAP') and [column_name] = 'performance_type'
GO


SELECT * FROM TR_PERF_TYPE


IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_TITLE_MOS_MAP_TU]'))
    DROP TRIGGER [dbo].[LTR_TITLE_MOS_MAP_TU]
GO

CREATE TRIGGER [dbo].[LTR_TITLE_MOS_MAP_TU] ON [dbo].[LTR_TITLE_MOS_MAP] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_TITLE_MOS_MAP]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_TITLE_MOS_MAP] a, inserted b
    WHERE	a.id = b.id
 
END
GO
