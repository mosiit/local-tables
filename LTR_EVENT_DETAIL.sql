USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LTR_EVENT_DETAIL](
	[id_key] [INT] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[campaign_no] [INT] NULL,
	[eventcode] [VARCHAR](30) NULL,
	[eventname] [VARCHAR](110) NULL,
	[evntlocat] [VARCHAR](55) NULL,
	[evnttype] [INT] NULL,
	[created_by] [CHAR](8) NULL CONSTRAINT [DF_LTR_EVENT_DETAIL_created_by]  DEFAULT ([dbo].[fs_user]()),
	[create_dt] [DATETIME] NULL CONSTRAINT [DF_LTR_EVENT_DETAIL_create_dt]  DEFAULT (GETDATE()),
	[last_updated_by] [CHAR](8) NULL CONSTRAINT [DF_LTR_EVENT_DETAIL_last_updated_by]  DEFAULT ([dbo].[fs_user]()),
	[last_update_dt] [DATETIME] NOT NULL CONSTRAINT [DF_LTR_EVENT_DETAIL_last_update_dt]  DEFAULT (GETDATE()),
	[create_loc] [VARCHAR](16) NULL CONSTRAINT [DF_LTR_EVENT_DETAIL_create_loc]  DEFAULT ([dbo].[fs_location]()),
 CONSTRAINT [PK_LTR_EVENT_DETAIL] PRIMARY KEY CLUSTERED 
(
	[id_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LTR_EVENT_DETAIL]  WITH CHECK ADD  CONSTRAINT [LTR_EVENT_DETAIL_FK_evnttype] FOREIGN KEY([evnttype])
REFERENCES [dbo].[LTR_EVENT_TYPE] ([id])
GO

ALTER TABLE [dbo].[LTR_EVENT_DETAIL] CHECK CONSTRAINT [LTR_EVENT_DETAIL_FK_evnttype]
GO

CREATE TRIGGER TG_LTR_EVENT_DETAIL_UPDATE ON dbo.LTR_EVENT_DETAIL 
FOR UPDATE 
AS 
UPDATE a
SET last_updated_by = dbo.fs_user(), last_update_dt = GETDATE()
FROM LTR_EVENT_DETAIL a 
JOIN inserted b 
	ON a.id_key = b.id_key
GO