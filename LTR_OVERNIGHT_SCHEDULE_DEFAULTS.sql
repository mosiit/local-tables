USE [impresario]
GO

    SET ANSI_NULLS ON
    SET ANSI_PADDING ON
    SET ANSI_WARNINGS ON
GO

    --DROP TABLE [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS]
GO

    CREATE TABLE [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS]  ([default_id] INT IDENTITY(1,1) NOT NULL,
                                                           [job_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                           [job_schedule]VARCHAR(100) NOT NULL DEFAULT (''),
                                                           [notes] VARCHAR(255) NOT NULL DEFAULT(''),
                                                           [create_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
                                                           [created_by] VARCHAR(10) NOT NULL DEFAULT([dbo].[fs_user]()),
                                                           [create_loc] VARCHAR(25) NOT NULL DEFAULT(HOST_NAME()),
                                                           [last_update_dt] [datetime] NULL DEFAULT (GETDATE()),
                                                           [last_updated_by] [varchar] (10) NULL DEFAULT ([dbo].[fs_user]()),
                                                           [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                                           [control_group]  INT NOT NULL DEFAULT(-1),
    CONSTRAINT [PK_LTR_OVERNIGHT_SCHEDULE_DEFAULTS] PRIMARY KEY NONCLUSTERED 
              ([default_id] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])  ON [PRIMARY]

GO

    GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] TO ImpUsers
GO

    IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERNIGHT_ACTIVITIES]') AND name = N'ix_LTR_OVERNIGHT_ACTIVITIES_activity_dt_activity_name')
        CREATE UNIQUE CLUSTERED INDEX [ix_LTR_OVERNIGHT_job_name] ON [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] ([job_name] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

    EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_OVERNIGHT_SCHEDULE_DEFAULTS'
GO
    
    IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS_TU]'))
        DROP TRIGGER [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS_TU]
GO

    CREATE TRIGGER [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS_TU] ON [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] FOR UPDATE
    AS BEGIN

        SET NOCOUNT ON 

        --Update the last_updated_by, last_update_dt columns
        IF NOT (UPDATE(last_update_dt)) AND NOT (UPDATE(last_updated_by))
            UPDATE 	[dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS]
            SET 	[last_updated_by] = dbo.FS_USER(), [last_update_dt] = getdate()
            FROM	[dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] a, inserted b
            WHERE	a.default_id = b.default_id
 
    END

GO

INSERT INTO [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS] ([job_name], [job_schedule], [notes])
VALUES ('Manager on Duty', '2:30 PM - Midnight', ''),
       ('Head Overnight', '3:30 PM - 11:00 AM', ''),
       ('Overnight', '3:30 PM - 11:00 AM', ''),
       ('Swing', '3:30 PM - Midnight', ''),
       ('Intern', '7:00 PM - 10:15 PM', ''),
       ('Volunteer', '4:00 PM - 10:15 PM', ''),
       ('Registration Assistant', '4:00 PM - 7:00 PM', '')

SELECT * FROM [dbo].[LTR_OVERNIGHT_SCHEDULE_DEFAULTS]
