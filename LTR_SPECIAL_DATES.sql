USE [impresario]
GO

/****** Object:  Trigger [LTR_SPECIAL_DATES_TU]    Script Date: 4/10/2020 9:32:59 AM ******/
DROP TRIGGER [dbo].[LTR_SPECIAL_DATES_TU]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [FK_LTR_SPECIAL_DATES_LTR_SPECIAL_DATES_CATEGORIES]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_last_updated_by]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_last_update_dt]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_create_loc]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_created_by]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_create_dt]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] DROP CONSTRAINT [DF_LTR_SPECIAL_DATES_inactive]
GO

/****** Object:  Index [IX_DateVenueCategory]    Script Date: 4/10/2020 9:32:59 AM ******/
DROP INDEX [IX_DateVenueCategory] ON [dbo].[LTR_SPECIAL_DATES] WITH ( ONLINE = OFF )
GO

/****** Object:  Table [dbo].[LTR_SPECIAL_DATES]    Script Date: 4/10/2020 9:32:59 AM ******/
DROP TABLE [dbo].[LTR_SPECIAL_DATES]
GO

/****** Object:  Table [dbo].[LTR_SPECIAL_DATES]    Script Date: 4/10/2020 9:32:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LTR_SPECIAL_DATES](
	[id] [int] NULL,
	[Date] [datetime] NOT NULL,
	[Venue] [varchar](300) NOT NULL,
	[Category] [varchar](100) NULL,
	[Notes] [varchar](500) NULL,
	[inactive] [char](1) NOT NULL,
	[create_dt] [datetime] NOT NULL,
	[created_by] [varchar](10) NOT NULL,
	[create_loc] [varchar](25) NOT NULL,
	[last_update_dt] [datetime] NOT NULL,
	[last_updated_by] [varchar](10) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_DateVenueCategory]    Script Date: 4/10/2020 9:32:59 AM ******/
CREATE CLUSTERED INDEX [IX_DateVenueCategory] ON [dbo].[LTR_SPECIAL_DATES]
(
	[Date] ASC,
	[Venue] ASC,
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_inactive]  DEFAULT ('N') FOR [inactive]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_create_dt]  DEFAULT (getdate()) FOR [create_dt]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_created_by]  DEFAULT ([dbo].[FS_USERNAME]()) FOR [created_by]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_create_loc]  DEFAULT (HOST_NAME()) FOR [create_loc]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_last_update_dt]  DEFAULT (GETDATE()) FOR [last_update_dt]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] ADD  CONSTRAINT [DF_LTR_SPECIAL_DATES_last_updated_by]  DEFAULT ([dbo].[FS_USERNAME]()) FOR [last_updated_by]
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES]  WITH CHECK ADD  CONSTRAINT [FK_LTR_SPECIAL_DATES_LTR_SPECIAL_DATES_CATEGORIES] FOREIGN KEY([Category])
REFERENCES [dbo].[LTR_SPECIAL_DATES_CATEGORIES] ([Category])
GO

ALTER TABLE [dbo].[LTR_SPECIAL_DATES] CHECK CONSTRAINT [FK_LTR_SPECIAL_DATES_LTR_SPECIAL_DATES_CATEGORIES]
GO

/****** Object:  Trigger [dbo].[LTR_SPECIAL_DATES_TU]    Script Date: 4/10/2020 9:32:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[LTR_SPECIAL_DATES_TU] ON [dbo].[LTR_SPECIAL_DATES] FOR UPDATE
AS BEGIN

 SET NOCOUNT ON 

 --Update the last_updated_by, last_update_dt columns
 UPDATE 	[dbo].[LTR_SPECIAL_DATES]
 SET 	last_updated_by = [dbo].FS_USERNAME(), last_update_dt = GETDATE()
 FROM	[dbo].[LTR_SPECIAL_DATES] a, inserted b
 WHERE	a.[Date] = b.[Date]
 AND	a.[Venue] = b.[Venue]
 AND	a.[Category] = b.[Category]
 
END

GO


