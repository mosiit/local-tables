USE [impresario]
GO

/****** Object:  Index [NCX_LTR_ENTITLEMENT_AutoJobIndex]    Script Date: 11/22/2019 9:51:34 AM ******/
DROP INDEX [NCX_LTR_ENTITLEMENT_AutoJobIndex] ON [dbo].[LTR_ENTITLEMENT]
GO

/****** Object:  Index [NCX_LTR_ENTITLEMENT_AutoJobIndex]    Script Date: 11/22/2019 9:51:34 AM ******/
CREATE NONCLUSTERED INDEX [NCX_LTR_ENTITLEMENT_AutoJobIndex] ON [dbo].[LTR_ENTITLEMENT]
(
	[memb_level_no] ASC,
	[entitlement_no] ASC,
	[ent_tkw_id] ASC,
	[ent_price_type_group] ASC,
	[reset_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


