USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LT_HISTORY_TRX_STATS]') AND type in (N'U'))
    DROP TABLE [dbo].[LT_HISTORY_TRX_STATS]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LT_HISTORY_TRX_STATS]') AND type in (N'U')) BEGIN
           CREATE TABLE [dbo].[LT_HISTORY_TRX_STATS](
	        [run_dt] [DATETIME] NOT NULL,
	        [history_dt] [DATETIME] NOT NULL,
	        [history_date] [CHAR](10) NOT NULL,
	        [user_id] [VARCHAR](10) NOT NULL,
	        [user_name] [VARCHAR](100) NOT NULL,
	        [user_name_sort] [VARCHAR](100) NOT NULL,
	        [user_location] [VARCHAR](30) NOT NULL,
	        [orders_created] [DECIMAL](18, 4) NOT NULL,
	        [orders_touched] [DECIMAL](18, 4) NOT NULL,
            [membership_sales] DECIMAL(18, 4) NOT NULL,
	        [basic_2_sales] [DECIMAL](18, 4) NOT NULL,
            [basic_5_sales] [DECIMAL](18, 4) NOT NULL,
            [basic_8_sales] [DECIMAL](18, 4) NOT NULL,
            [basic_total_sales] [DECIMAL](18, 4) NOT NULL,
            [onestep_basic] [DECIMAL](18, 4) NOT NULL,
	        [premier_2_sales] [DECIMAL](18, 4) NOT NULL,
            [premier_5_sales] [DECIMAL](18, 4) NOT NULL,
            [premier_8_sales] [DECIMAL](18, 4) NOT NULL,
            [premier_total_sales] [DECIMAL](18, 4) NOT NULL,
	        [onestep_premier] [DECIMAL](18, 4) NOT NULL,
            [onestep_total] [DECIMAL](18, 4) NOT NULL,
            [orders_counted] [DECIMAL](18, 4) NOT NULL,
            [multi_performances_orders] [DECIMAL](18, 4) NOT NULL,
	        [performances_sold] [DECIMAL](18, 4) NOT NULL,
         CONSTRAINT [PK_LT_HISTORY_TRX_STATS] PRIMARY KEY CLUSTERED ([history_dt] ASC, [user_id] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
END
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TRX_STATS_history_date_user_id] ON [dbo].[LT_HISTORY_TRX_STATS] ([history_date] ASC, [user_id] ASC)
       WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


GRANT SELECT ON [dbo].[LT_HISTORY_TRX_STATS] TO ImpUsers
GO


--SELECT * FROM dbo.LT_HISTORY_TRX_STATS
--SELECT max(history_dt) FROM dbo.LT_HISTORY_TRX_STATS

