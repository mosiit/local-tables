USE [impresario]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] DROP CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_last_update_dt]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] DROP CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_last_updated_by]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] DROP CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_create_loc]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] DROP CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_create_dt]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] DROP CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_created_by]
GO

/****** Object:  Table [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG]    Script Date: 12/6/2016 1:24:27 PM ******/
DROP TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG]
GO

/****** Object:  Table [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG]    Script Date: 12/6/2016 1:24:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG](
	[status] [varchar](30) NOT NULL,
	[error_msg] [varchar](4000) NULL,
	[month_year] [varchar](30) NOT NULL,
	[customer_no] [varchar](30) NOT NULL,
	[customer_name] [varchar](300) NULL,
	[account_type] [int] NOT NULL,
	[mill_exp_date] [varchar](4) NULL,
	[sage_exp_date] [varchar](4) NULL,
	[exp_date_used] [datetime] NOT NULL,
	[created_by] [varchar](8) NULL,
	[created_dt] [datetime] NULL,
	[created_loc] [varchar](16) NULL,
	[last_updated_by] [varchar](8) NULL,
	[last_updated_dt] [datetime] NULL,
 CONSTRAINT [PK_LT_TOKENIZE_CREDIT_CARD_LOG] PRIMARY KEY CLUSTERED 
(
	[month_year] ASC,
	[customer_no] ASC,
	[account_type] ASC,
	[exp_date_used] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] ADD  CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] ADD  CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_create_dt]  DEFAULT (getdate()) FOR [created_dt]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] ADD  CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_create_loc]  DEFAULT (host_name()) FOR [created_loc]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] ADD  CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
GO

ALTER TABLE [dbo].[LT_TOKENIZE_CREDIT_CARD_LOG] ADD  CONSTRAINT [DF_LT_TOKENIZE_CREDIT_CARD_LOG_last_update_dt]  DEFAULT (getdate()) FOR [last_updated_dt]
GO


