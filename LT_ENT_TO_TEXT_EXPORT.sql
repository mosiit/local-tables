USE [impresario]
GO

/****** Object:  Table [dbo].[LT_ENT_TO_TEXT_EXPORT]    Script Date: 1/9/2019 8:17:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_ENT_TO_TEXT_EXPORT](
	[venue_name] [varchar](1000) NULL,
	[exp_dt] [varchar](1000) NULL,
	[cust_name] [varchar](1000) NULL,
	[resale_stub] [varchar](1000) NULL,
	[where_to1] [varchar](1000) NULL,
	[where_to2] [varchar](1000) NULL,
	[gc1] [varchar](1000) NULL,
	[gc2] [varchar](1000) NULL,
	[gc3] [varchar](1000) NULL,
	[valid1] [varchar](1000) NULL,
	[valid2] [varchar](1000) NULL,
	[ff] [varchar](1000) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


