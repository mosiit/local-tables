USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_TEMPORARY_EXHIBITS](
	[ID] INT NOT NULL IDENTITY, 
	[exhibit_name] [VARCHAR](255) NULL,
	[opening_date] DATE,
	[closing_date] DATE,
	[notes] [VARCHAR](255) NULL,
	[surcharge] bit DEFAULT 0 ,
	[created_by] [CHAR](8) NULL CONSTRAINT [DF_TEMP_EXH_created_by]  DEFAULT (USER_NAME()),
	[create_dt] [DATETIME] NULL CONSTRAINT [DF_TEMP_EXH_create_dt]  DEFAULT (GETDATE()),
	[last_updated_by] [CHAR](8) NULL CONSTRAINT [DF_TEMP_EXH_updated_by]  DEFAULT (USER_NAME()),
	[last_update_dt] [DATETIME] NULL CONSTRAINT [DF_TEMP_EXH_updated_dt]  DEFAULT (GETDATE()),
	[inactive] [CHAR](1) NULL CONSTRAINT [DF_TEMP_EXH_inactive]  DEFAULT ('N')
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--------------------
--INSERT INTO dbo.LT_TEMPORARY_EXHIBITS
--(
--   [exhibit_name],
--	[opening_date],
--	[closing_date]
--)
--SELECT 
--	[Exhibit name],
--    [Opening Date],
--    [Closing date]
--FROM admin.dbo.LT_TEMPORARY_EXHIBITS_old
--WHERE [Exhibit name] <> 'Secret World Inside You (CANCELLED)'

--UPDATE LT_TEMPORARY_EXHIBITS
--SET surcharge = 1
--WHERE id = 21 -- Body Worlds

--SELECT * FROM dbo.LT_TEMPORARY_EXHIBITS
