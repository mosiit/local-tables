USE [impresario]
GO

DROP TABLE [dbo].[LTR_ORDER_CAT_FOR_PARAMETER]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LTR_ORDER_CAT_FOR_PARAMETER](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NULL,
	[category_desc] [varchar](30) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

insert LTR_ORDER_CAT_FOR_PARAMETER 
(category_id,category_desc) 
select id,description 
from TR_ORDER_CATEGORY
where id >= 0 

grant select, insert,delete,references on [dbo].[LTR_ORDER_CAT_FOR_PARAMETER] to ImpUsers
exec UP_POPULATE_REFERENCE_METADATA @table_name = 'LTR_ORDER_CAT_FOR_PARAMETER'