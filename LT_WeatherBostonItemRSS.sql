USE [impresario]
GO

/****** Object:  Table [dbo].[LT_WeatherBostonItemRSS]    Script Date: 7/12/2019 11:38:54 AM ******/
DROP TABLE [dbo].[LT_WeatherBostonItemRSS]
GO

/****** Object:  Table [dbo].[LT_WeatherBostonItemRSS]    Script Date: 7/12/2019 11:38:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LT_WeatherBostonItemRSS](
	[item_Id] [INT] NULL,
	[title] [NVARCHAR](4000) NULL,
	[link] [NVARCHAR](4000) NULL,
	[description] [NVARCHAR](4000) NULL,
	[channel_Id] [INT] NULL,
	[ModifiedDate] [DATETIME] NULL
) ON [PRIMARY]

GO


