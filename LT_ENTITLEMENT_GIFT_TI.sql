USE [impresario]
GO

/****** Object:  Trigger [dbo].[LT_ENTITLEMENT_GIFT_TI]    Script Date: 7/9/2019 11:21:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tessitura Network Jon Ballinger
-- Create date: 2/9/2018
-- Description:	insert  trigger for LT_ENTITLEMENT_GIFT, depending on the entitlement gift level, this  process can loop over itself.  Initial
-- entry that cause the gift to be created is deleted. only recursed created records remain.  Context variables are used to indicate where and how processing is happening
-- so that update triggers know how to behave.
-- edited 7/9/2019 to allow for use of pending entitlements
-- =============================================

ALTER TRIGGER [dbo].[LT_ENTITLEMENT_GIFT_TI] ON [dbo].[LT_ENTITLEMENT_GIFT] FOR INSERT

AS

SET NOCOUNT ON


declare @value varchar(500)
select @value=dbo.FS_GET_PARAM_FROM_APPNAME('CT')

	
	declare @tess_userid char(8),		
	@tess_ugid char(8)

	Select @tess_userid = userid
	From [dbo].T_METUSER 
	Where db_userid = dbo.FS_USER()

	Select @tess_ugid = Coalesce([dbo].FS_GET_PARAM_FROM_APPNAME('UG'), 'admin')
	
	

	--Check for max entitlements	
	if exists( select 1 
				from inserted a
				join LTX_CUST_ENTITLEMENT b on a.ltx_cust_entitlement_id = b.id_key 				
				and a.num_items >b.num_ent and @value is not null)
		begin
				rollback transaction         
				raiserror ('Number of gift entered can not be greated than number of available entitlements.', 16, 1)
				return
		end 

	if exists( select 1 
				from inserted a
				join LTX_CUST_ENTITLEMENT b on a.ltx_cust_entitlement_id = b.id_key 
				join LTR_ENTITLEMENT c on c.entitlement_no = b.entitlement_no
				and a.num_items >c.max_num_ent and @value is not null)
	 begin
			if not exists(select 1 from T_MGR_RIGHTS
						where USERID=@tess_userid and ug_id=@tess_ugid)
				begin
				rollback transaction         
				raiserror ('Number of gift entered can not be greated than max allowed for entitlement.', 16, 1)
				return
				end
		 
		end

		if exists( select 1 
				from inserted a
				where a.gift_status='C' and a.gift_customer_no is  not null)
		begin
					rollback transaction         
					raiserror ('Gift customer can only be assigned when gift is being accepted.', 16, 1)
					return
		end 

	if exists( select 1 
				from inserted a
				where a.gift_status='A' and a.gift_customer_no is null)
		begin
					rollback transaction         
					raiserror ('Gifts can only be accepted with gift recipient customer number.', 16, 1)
					return
		end 


if ( @value is not null )
begin

	exec ap_set_context 'gift_tg', 'Y'
	declare @from_tg varchar(500)
	select @from_tg=dbo.fs_get_param_from_appname('gift_tg')



	declare @num_items int	
	select @num_items =a.num_items
	from inserted a
	 join LTX_CUST_ENTITLEMENT c on c.id_key = a.ltx_cust_entitlement_id
		and a.custom_screen_ctrl is null
	 join LTR_ENTITLEMENT b on c.entitlement_no = b.entitlement_no
	 and b.gift_level='S'

	 
	 if exists(select 1 from LTR_ENTITLEMENT a join inserted b on a.entitlement_no = b.entitlement_no and coalesce(a.transferrable,'N')='N')
	 begin
				rollback transaction
				raiserror ('The entitlement is not transferrable.', 16, 1)
				return
		end

	
	 if @num_items > 0
	 begin
	 
		if exists (select cnt 
				from (select num_items, dbo.[LFS_GET_ENTITLEMENT_REMAINING_COUNT](ltx_cust_entitlement_id) cnt
					from inserted) a where num_items>cnt)
			begin
					rollback transaction
					raiserror ('The number of gifts is more then what is available.', 16, 1)
					return
			end
	
		exec ap_set_context 'gift_tg_m', 'Y';

 		with    cte(
				[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],
			[num_items],
			[entitlement_no],
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			id_key,
			 i) as 
			(
			select  
			a.[customer_no],
			a.[gift_customer_no],
			a.[gift_recip_email],
			a.[gift_recip_name],
			a.[gift_message],
			a.[gift_status],
			a.[li_seq_no],
			a.[num_items],
			b.[entitlement_no],
			a.[ltx_cust_entitlement_id],
			--init_dt = case when d.current_status=3 then getdate() else coalesce(d.init_dt, c.init_dt) end, --7/7/19 original
			init_dt = case when b.is_adhoc = 'y' then c.init_dt else getdate() end, --changed 7/7/2019 to allow for gifting of pending entitlements
			coalesce(d.expr_dt,c.expr_dt),	
			a.[created_by],
			a.[create_dt],
			a.[last_updated_by],
			a.[last_update_dt],
			a.id_key,
			1
			from  inserted a
			join dbo.LTX_CUST_ENTITLEMENT c on a.ltx_cust_entitlement_id = c.id_key 
				and a.custom_screen_ctrl is null			
			join LTR_ENTITLEMENT b on b.entitlement_no = c.entitlement_no
				and b.gift_level='S'
			left join TX_CUST_MEMBERSHIP d on c.cust_memb_no = d.cust_memb_no	
			union all
			select 
			[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],
			[num_items],
			[entitlement_no],
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			id_key,
			i + 1
			from    cte
			where   cte.i < cte.[num_items]
			)
	insert into LT_ENTITLEMENT_GIFT(
			--id_key,
			[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],
			[num_items],
			[entitlement_no],
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			custom_screen_ctrl,
			gift_code)

	select  
			--i+@next_id_ct,
			[customer_no],
			[gift_customer_no],
			[gift_recip_email],
			[gift_recip_name],
			[gift_message],
			[gift_status],
			[li_seq_no],
			1,
			[entitlement_no],
			[ltx_cust_entitlement_id],
			[init_dt],
			[expr_dt] ,
			[created_by],
			[create_dt],
			[last_updated_by],
			[last_update_dt],
			id_key,
			SUBSTRING(
							CONVERT(varchar(255), NEWID()),
							10,
							5) 
					+ CONVERT(char(4), RIGHT(id_key,4))
					+ SUBSTRING (
							convert(varchar(255), NEWID()),
							19,
							5) 
	 from    cte


	 	UPDATE 	a
			SET a.group_gift_code= coalesce(a.group_gift_code,a.gift_code)				
			FROM  LT_ENTITLEMENT_GIFT a
		join inserted b on a.ltx_cust_entitlement_id = b.ltx_cust_entitlement_id
		join ltx_cust_entitlement c on c.id_key=b.ltx_cust_entitlement_id
		and a.custom_screen_ctrl is not null
		
		exec ap_set_context 'gift_tg', 'Y'
		
		INSERT dbo.LTX_CUST_ORDER_ENTITLEMENT (
			customer_no,				
			ltx_cust_entitlement_id,
			entitlement_no,
			entitlement_date,
			num_used,
			gift_code,
			cust_memb_no
			)
		SELECT
			a.customer_no,
			a.ltx_cust_entitlement_id,
			a.entitlement_no,
			getdate(),
			1,
			a.gift_code,
			c.cust_memb_no
		FROM  LT_ENTITLEMENT_GIFT a
		join inserted b on a.ltx_cust_entitlement_id = b.ltx_cust_entitlement_id
		join ltx_cust_entitlement c on c.id_key=b.ltx_cust_entitlement_id
		and a.custom_screen_ctrl  in (select id_key from inserted) 
		
		
	

		INSERT dbo.LTX_CUST_ENTITLEMENT (
		customer_no,
		cust_memb_no,
		entitlement_no,
		num_ent,
		gift_no,
		init_dt,
		expr_dt)
		SELECT 
			a.gift_customer_no,
			0,
			a.entitlement_no,
			1,
			a.id_key,
			a.init_dt,
			a.expr_dt
		FROM dbo.LT_ENTITLEMENT_GIFT a
			JOIN inserted b on a.ltx_cust_entitlement_id = b.ltx_cust_entitlement_id		
			and a.custom_screen_ctrl=b.id_key		
		WHERE a.gift_status = 'A' 
		and a.gift_customer_no IS NOT NULL
		and a.custom_screen_ctrl is not null

	


		 delete from LT_ENTITLEMENT_GIFT
		 where id_key in (select id_key from inserted) 
	 
		  exec ap_set_context 'gift_tg', 'N'
		  exec ap_set_context 'gift_tg_m', 'N'
	 

	 end 

	 else
		Begin		
		
			declare @from_tg_m varchar(500)
			select @from_tg_m=dbo.fs_get_param_from_appname('gift_tg_m')

			if @from_tg_m='N' 
				and exists (select cnt 
					from (select num_items, dbo.[LFS_GET_ENTITLEMENT_REMAINING_COUNT](ltx_cust_entitlement_id) cnt
					from inserted) a where num_items>cnt)
				begin
					 rollback transaction
					 raiserror ('The numebr of gifts is more then what is available.', 16, 1)
					 return
				end
		
			UPDATE 	a
			SET
				gift_code = SUBSTRING(
									CONVERT(varchar(255), NEWID()),
									10,
									5) 
							+ CONVERT(char(4), RIGHT(b.id_key,4))
							+ SUBSTRING (
									convert(varchar(255), NEWID()),
									19,
									5) ,				
				 a.entitlement_no = c.entitlement_no,
				 --a.init_dt=coalesce(c.init_dt,d.init_dt), --original
				 a.init_dt = case when e.is_adhoc = 'y' then c.init_dt else getdate() end, --updated 7/7/2019
				 a.expr_dt=coalesce(c.expr_dt,d.expr_dt)
			FROM dbo.LT_ENTITLEMENT_GIFT a
				JOIN inserted b ON a.id_key = b.id_key
				join LTX_CUST_ENTITLEMENT c on c.id_key = b.ltx_cust_entitlement_id			
				left join TX_CUST_MEMBERSHIP d on c.cust_memb_no = d.cust_memb_no		
				join LTR_ENTITLEMENT e on e.entitlement_no =c.entitlement_no
			WHERE a.gift_code IS NULL;
			
			exec ap_set_context 'gift_tg', 'Y'

			
			UPDATE 	a
			SET a.group_gift_code= coalesce(a.group_gift_code,a.gift_code)				
			FROM dbo.LT_ENTITLEMENT_GIFT a
				JOIN inserted b ON a.id_key = b.id_key
				join LTX_CUST_ENTITLEMENT c on c.id_key = b.ltx_cust_entitlement_id				
			
			exec ap_set_context 'gift_tg', 'Y'

			
			INSERT dbo.LTX_CUST_ORDER_ENTITLEMENT (
				customer_no,				
				ltx_cust_entitlement_id,
				entitlement_no,
				entitlement_date,
				num_used,
				gift_code,
				cust_memb_no
				)
			SELECT
				a.customer_no,
				a.ltx_cust_entitlement_id,
				a.entitlement_no,
				getdate(),
				a.num_items,
				a.gift_code,
				c.cust_memb_no
			FROM  LT_ENTITLEMENT_GIFT a
			join inserted b on a.ltx_cust_entitlement_id = b.ltx_cust_entitlement_id
				join ltx_cust_entitlement c on c.id_key=b.ltx_cust_entitlement_id
					and b.id_key=a.id_key
	
				INSERT dbo.LTX_CUST_ENTITLEMENT (
				customer_no,
				cust_memb_no,
				entitlement_no,
				num_ent,
				gift_no,
				init_dt,
				expr_dt)
				SELECT 
					a.gift_customer_no,
					0,
					a.entitlement_no,
					a.num_items,
					a.id_key,
					a.init_dt,
					a.expr_dt
				FROM dbo.LT_ENTITLEMENT_GIFT a
					JOIN inserted b ON a.id_key = b.id_key
					LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce ON ce.gift_no = a.id_key
				WHERE a.gift_status = 'A' 
				and a.gift_customer_no IS NOT NULL
				AND ce.gift_no IS NULL /* ce row does not exist */

				exec ap_set_context 'gift_tg', 'N'
		end

	end
else
Begin
			UPDATE 	a
			SET
				gift_code = SUBSTRING(
									CONVERT(varchar(255), NEWID()),
									10,
									5) 
							+ CONVERT(char(4), RIGHT(b.id_key,4))
							+ SUBSTRING (
									convert(varchar(255), NEWID()),
									19,
									5) ,
				 a.entitlement_no = c.entitlement_no,
				 --init_dt = case when d.current_status=3 then getdate() else coalesce(d.init_dt, c.init_dt) end, --original
				  init_dt = case when e.is_adhoc = 'y' then c.init_dt else getdate() end, --updated 7/7/2019
				 a.expr_dt=coalesce(c.expr_dt,d.expr_dt)
			FROM dbo.LT_ENTITLEMENT_GIFT a
				JOIN inserted b ON a.id_key = b.id_key
				join LTX_CUST_ENTITLEMENT c on c.id_key = b.ltx_cust_entitlement_id				
				join LTR_ENTITLEMENT e on e.entitlement_no = c.entitlement_no
				left join TX_CUST_MEMBERSHIP d on c.cust_memb_no = d.cust_memb_no	
			WHERE a.gift_code IS NULL;

			UPDATE 	a
			SET a.group_gift_code= coalesce(a.group_gift_code,a.gift_code)				
			FROM dbo.LT_ENTITLEMENT_GIFT a
				JOIN inserted b ON a.id_key = b.id_key
				join LTX_CUST_ENTITLEMENT c on c.id_key = b.ltx_cust_entitlement_id			
					
		

		
end


GO


