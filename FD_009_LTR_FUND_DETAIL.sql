USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_FUND_DETAIL]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_FUND_DETAIL](
	    [fund_no] [int] NOT NULL,
	    [acct_goal_no] [int] NULL,
    	[acct_grp_no] [int] NULL,
	    [cpf_flag] [char](1) NULL,
    	[exh_prg_cat_no] [int] NULL,
	    [full_fund] [varchar](80) NULL,
    	[printable_fund] [varchar](80) NULL,
	    [overall_cm_no] [int] NULL,
    	[cm_category1] [int] NULL,
	    [cm_category2] [int] NULL,
    	[cm_category3] [int] NULL,
        [designation] [int] NULL,
	    [notes] [varchar](255) NULL,
    	[inactive] [char](1) NULL,
	    [created_by] [varchar](8) NULL,
    	[create_dt] [datetime] NULL,
	    [create_loc] [varchar](16) NULL,
    	[last_updated_by] [varchar](8) NULL,
	    [last_update_dt] [datetime] NULL,
    CONSTRAINT [PK_LTR_FUND_DETAIL] PRIMARY KEY CLUSTERED ([fund_no] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_fund_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_fund_no]  DEFAULT ((0)) FOR [fund_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_acct_goal_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_acct_goal_no]  DEFAULT ((0)) FOR [acct_goal_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_acct_grp_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_acct_grp_no]  DEFAULT ((0)) FOR [acct_grp_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_cpf_flag]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_cpf_flag]  DEFAULT ('') FOR [cpf_flag]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_exh_prg_cat_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_exh_prg_cat_no]  DEFAULT ((0)) FOR [exh_prg_cat_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_full_fund]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_full_fund]  DEFAULT ('') FOR [full_fund]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_printable_fund]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_printable_fund]  DEFAULT ('') FOR [printable_fund]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_overall_cm_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_overall_cm_no]  DEFAULT ((0)) FOR [overall_cm_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_cm_category1]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_cm_category1]  DEFAULT ((0)) FOR [cm_category1]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_cm_category2]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_cm_category2]  DEFAULT ((0)) FOR [cm_category2]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_cm_category3]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_cm_category3]  DEFAULT ((0)) FOR [cm_category3]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_designation]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_designation]  DEFAULT ((0)) FOR [designation]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_notes]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_notes]  DEFAULT ('') FOR [notes]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_FUND_DETAIL_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_FUND_DETAIL] ADD  CONSTRAINT [DF_LTR_FUND_DETAIL_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_FUND_DETAIL] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_FUND_DETAIL'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_FUND_DETAIL_TU]'))
    DROP TRIGGER [dbo].[LTR_FUND_DETAIL_TU]
GO

CREATE TRIGGER [dbo].[LTR_FUND_DETAIL_TU] ON [dbo].[LTR_FUND_DETAIL] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_FUND_DETAIL]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_FUND_DETAIL] a, inserted b
    WHERE	a.fund_no = b.fund_no
 
END
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'T_FUND', [dddw_value] = 'fund_no', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'fund_no'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_ACCT_GOAL', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'acct_goal_no'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_ACCT_GRP', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'acct_grp_no'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_EXH_PRG_CAT', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'exh_prg_cat_no'
GO    
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_OVERALL_CAMPAIGN', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'overall_cm_no'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_CM_CATEGORY1', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'cm_category1'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_CM_CATEGORY2', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'cm_category2'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_CM_CATEGORY3', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'cm_category3'
GO
UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'TR_CONT_DESIGNATION', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_FUND_DETAIL') and [column_name] = 'designation'
GO
