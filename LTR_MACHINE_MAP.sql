USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP]') AND type in (N'U'))
    DROP TABLE [dbo].[LTR_MACHINE_MAP]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_MACHINE_MAP](
	    [id] [int] NOT NULL,
        [machine_name] [varchar] (30) NOT NULL,
        [machine_description] [varchar] (50) NOT NULL,
        [machine_notes] [varchar] (255) NULL,
        [inactive] [char](1) NOT NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_MACHINE_MAP] PRIMARY KEY NONCLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'uq_LTR_LTR_MACHINE_MAP_machine_name')
    DROP INDEX [uq_LTR_MACHINE_MAP_machine_name] ON [dbo].[LTR_MACHINE_MAP]
GO

CREATE UNIQUE CLUSTERED INDEX [uq_LTR_MACHINE_MAP_machine_name] ON [dbo].[LTR_MACHINE_MAP] ([machine_name] ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_machine_name]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_machine_name]  DEFAULT ('') FOR [machine_name]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP_machine_description]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_machine_description]  DEFAULT ('') FOR [machine_description]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP_machine_notes]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_machine_notes]  DEFAULT ('') FOR [machine_notes]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_MACHINE_MAP_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_MACHINE_MAP] ADD  CONSTRAINT [DF_LTR_MACHINE_MAP_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_MACHINE_MAP] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_MACHINE_MAP'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_MACHINE_MAP_TU]'))
    DROP TRIGGER [dbo].[LTR_MACHINE_MAP_TU]
GO

CREATE TRIGGER [dbo].[LTR_MACHINE_MAP_TU] ON [dbo].[LTR_MACHINE_MAP] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_MACHINE_MAP]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_MACHINE_MAP] a, inserted b
    WHERE	a.id = b.id
 
END
GO

INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (1,  'Box Office #01', 'VSO-1-W7-1682', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (2,  'Box Office #02', 'VSO-1-W7-1455', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (3,  'Box Office #03', 'VSO-1-W7-1683', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (4,  'Box Office #04', 'VSO-1-W7-1404', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (5,  'Box Office #05', 'VSO-1-W7-1405', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (6,  'Box Office #06', 'VSO-1-W7-1406', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (7,  'Box Office #07', 'VSO-1-W7-1407', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (8,  'Box Office #08', 'VSO-1-W7-1408', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (9,  'Box Office #09', 'VSO-1-W7-1409', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (10, 'Box Office #10', 'VSO-1-W7-1410', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (11, 'Box Office #11', 'VSO-1-W7-1411', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (12, 'Box Office #12', 'VSO-1-W7-1412', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (13, 'Box Office #13', 'VSO-1-W7-1684', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (14, 'Box Office #14', 'VSO-1-W7-1402', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (15, 'Box Office #15', 'VSO-1-W7-1403', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (16, 'Box Office #16', 'VSO-1-W7-1843', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (17, 'Box Office #17', 'VSO-1-W7-1844', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (18, 'Box Office #18', 'VSO-1-W7-1845', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (19, 'Box Office #19', 'VSO-1-W7-1855', 'Box Office, Main Lobby')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (20, 'Thrill Ride A',  'VSO-1-W7-1110', 'Atrium, Red Wing')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (21, 'Thrill Ride B',  'VSO-1-W7-1413', 'Atrium, Red Wing')
INSERT INTO LTR_MACHINE_MAP ([id], [machine_description], [machine_name], [machine_notes]) VALUES (22, 'Live Web Store', 'MCCOY', 'Web Store/Kiosk')
GO

SELECT * FROM [dbo].[LTR_MACHINE_MAP] ORDER BY [machine_description]

