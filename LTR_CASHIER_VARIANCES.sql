USE [impresario]
GO

/****** Object:  Table [dbo].[LTR_CASHIER_VARIANCES]    Script Date: 4/15/2016 3:13:00 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--DROP TABLE [dbo].[LTR_CASHIER_VARIANCES]
--GO

CREATE TABLE [dbo].[LTR_CASHIER_VARIANCES](
	[id] [int] IDENTITY(1,1) NOT NULL,
    [operator_location] [varchar](30) NULL DEFAULT (''),
	[operator_id] [varchar](8) NULL DEFAULT (''),
	[operator_first_name] [varchar](50) NULL DEFAULT (''),
	[operator_last_name] [varchar](75) NULL DEFAULT (''),
	[variance_date] [varchar](10) NULL DEFAULT (''),
    [variance_type] [varchar] (30) NULL DEFAULT (''),
	[variance_amount] [decimal] (18,2) NULL DEFAULT (0.0),
    [variance_reason] [int] NULL DEFAULT (2),
    [variance_status] [varchar] (30) NULL DEFAULT (''),
    [variance_notes] [varchar] (255) NULL DEFAULT (''),
    [inactive] char(1) NULL DEFAULT ('N'),
	[created_by] varchar(8) NULL DEFAULT ([dbo].[fs_user]()),
	[create_dt] [datetime] NULL DEFAULT (GETDATE()),
	[create_loc] varchar(16) NULL DEFAULT (host_name()),
	[last_updated_by] varchar(8) NULL DEFAULT ([dbo].[fs_user]()),
	[last_update_dt] [datetime] NULL DEFAULT (GETDATE()),
    [variance_unique_identifier] VARCHAR(70) NULL DEFAULT (''),
CONSTRAINT [PK_LTR_CASHIER_VARIANCES] PRIMARY KEY NONCLUSTERED 
([id] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


GRANT SELECT, INSERT, UPDATE ON [dbo].[LTR_CASHIER_VARIANCES] TO [ImpUsers]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHIER_VARIANCES]') AND name = N'ix_LTR_CASHIER_VARIANCES_variance_unique_identifier')
    CREATE UNIQUE CLUSTERED INDEX [ix_LTR_CASHIER_VARIANCES_variance_unique_identifier] ON [dbo].[LTR_CASHIER_VARIANCES] ([variance_unique_identifier] ASC)
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHIER_VARIANCES]') AND name = N'ix_LTR_CASHIER_VARIANCES_operator_id')
    CREATE NONCLUSTERED INDEX [ix_LTR_CASHIER_VARIANCES_operator_id] ON [dbo].[LTR_CASHIER_VARIANCES] ([operator_id] ASC)
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_CASHIER_VARIANCES'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'LTR_CASHIER_VARIANCE_REASONS', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHIER_VARIANCES') and [column_name] = 'variance_reason'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'TR_GOOESOFT_DROPDOWN', [dddw_value] = 'description', [dddw_description] = 'description', [dddw_where] = 'code = 1497 and inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHIER_VARIANCES') and [column_name] = 'variance_status'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [inactive] = 'Y' 
WHERE reference_table_id = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHIER_VARIANCES') AND [column_name] = 'variance_unique_identifier'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [column_type] = 'display'
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHIER_VARIANCES') 
  AND [column_type] = 'editable' AND [column_name] NOT IN ('inactive', 'variance_amount', 'variance_reason', 'variance_status', 'variance_notes')

--Inactivate the variance_status column but leave it in there in case they decide to use it at a future date
UPDATE dbo.[TR_REFERENCE_COLUMN] SET [inactive] = 'Y'
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHIER_VARIANCES')
  AND [column_name] = 'variance_status'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHIER_VARIANCES_TU]'))
    DROP TRIGGER [dbo].[LTR_CASHIER_VARIANCES_TU]
GO

CREATE TRIGGER [dbo].[LTR_CASHIER_VARIANCES_TU] ON [dbo].[LTR_CASHIER_VARIANCES] FOR UPDATE
AS BEGIN

    SET NOCOUNT ON

    IF UPDATE([last_updated_by]) RETURN

    DECLARE @audit_dt DATETIME = GETDATE(),  @audit_user VARCHAR(50) = [dbo].[FS_USER]()
    DECLARE @var_id INT, @var_amount DECIMAL(18,2)

    DECLARE @new_values TABLE ([id] int, [variance_unique_identifier] VARCHAR(50), [inactive] CHAR(1), [variance_type] VARCHAR(50), [variance_amount] DECIMAL(18,2),
                             [variance_reason] INT, [variance_status] VARCHAR(50), [variance_notes] VARCHAR(100))

    DECLARE @original_values TABLE ([id] int, [variance_unique_identifier] VARCHAR(50), [inactive] CHAR(1), [variance_type] VARCHAR(50), [variance_amount] DECIMAL(18,2),
                             [variance_reason] INT, [variance_status] VARCHAR(50), [variance_notes] VARCHAR(100))

    INSERT INTO @new_values (id, variance_unique_identifier, inactive, variance_type, variance_amount, variance_reason, variance_status, variance_notes)
    SELECT id, variance_unique_identifier, inactive, variance_type, variance_amount, variance_reason, variance_status, variance_notes FROM inserted

    INSERT INTO @original_values (id, variance_unique_identifier, inactive, variance_type, variance_amount, variance_reason, variance_status, variance_notes)
    SELECT id, variance_unique_identifier, inactive, variance_type, variance_amount, variance_reason, variance_status, variance_notes FROM deleted

    INSERT INTO [dbo].[LTA_CASHIER_VARIANCES_AUDIT] ([audit_timestamp], [audit_user], [audit_type], [variance_id], [variance_unique_identifier], [column_changed], [original_value], [changed_value])
    SELECT @audit_dt, @audit_user, 'U', d.[id], d.[variance_unique_identifier], 'inactive', d.inactive, i.inactive
    FROM @original_values AS d
         LEFT OUTER JOIN @new_values as i ON i.[variance_unique_identifier] = d.[variance_unique_identifier]
    WHERE d.inactive <> i.inactive

    INSERT INTO [dbo].[LTA_CASHIER_VARIANCEs_AUDIT] ([audit_timestamp], [audit_user], [audit_type], [variance_id], [variance_unique_identifier], [column_changed], [original_value], [changed_value])
    SELECT @audit_dt, @audit_user, 'U', d.[id], d.[variance_unique_identifier], 'variance_amount', CONVERT(VARCHAR(100),d.variance_amount), CONVERT(VARCHAR(100),i.variance_amount)
    FROM @original_values AS d
         LEFT OUTER JOIN @new_values as i ON i.[variance_unique_identifier] = d.[variance_unique_identifier]
    WHERE d.variance_amount <> i.variance_amount

    INSERT INTO [dbo].[LTA_CASHIER_VARIANCEs_AUDIT] ([audit_timestamp], [audit_user], [audit_type], [variance_id], [variance_unique_identifier], [column_changed], [original_value], [changed_value])
    SELECT @audit_dt, @audit_user, 'U', d.[id], d.[variance_unique_identifier], 'variance_status', d.variance_status, i.variance_status
    FROM @original_values AS d
         LEFT OUTER JOIN @new_values as i ON i.[variance_unique_identifier] = d.[variance_unique_identifier]
    WHERE d.variance_status <> i.variance_status
      
    INSERT INTO [dbo].[LTA_CASHIER_VARIANCEs_AUDIT] ([audit_timestamp], [audit_user], [audit_type], [variance_id], [variance_unique_identifier], [column_changed], [original_value], [changed_value])
    SELECT @audit_dt, @audit_user, 'U', d.[id], d.[variance_unique_identifier], 'variance_notes', d.variance_notes, i.variance_notes
    FROM @original_values AS d
         LEFT OUTER JOIN @new_values as i ON i.[variance_unique_identifier] = d.[variance_unique_identifier]
    WHERE d.variance_notes <> i.variance_notes

    INSERT INTO [dbo].[LTA_CASHIER_VARIANCEs_AUDIT] ([audit_timestamp], [audit_user], [audit_type], [variance_id], [variance_unique_identifier], [column_changed], [original_value], [changed_value])
    SELECT @audit_dt, @audit_user, 'U', d.[id], d.[variance_unique_identifier], 'variance_reason', ro.[description] + ' (' + CONVERT(VARCHAR(5),d.variance_reason) + ')', rn.[description] + ' (' + CONVERT(VARCHAR(5),i.variance_reason) + ')'
    FROM @original_values AS d
         LEFT OUTER JOIN @new_values as i ON i.[variance_unique_identifier] = d.[variance_unique_identifier]
         LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCE_REASONS] AS ro ON ro.[id] = d.[variance_reason]
         LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCE_REASONS] AS rn ON rn.[id] = i.[variance_reason]
    WHERE d.[variance_reason] <> i.[variance_reason]
    
    DECLARE amount_cursor INSENSITIVE CURSOR FOR
    SELECT [id], [variance_amount] FROM @new_values
    OPEN amount_cursor
    FETCH NEXT FROM amount_cursor INTO @var_id, @var_amount
    WHILE @@FETCH_STATUS <> -1 BEGIN

        UPDATE [dbo].[LTR_CASHIER_VARIANCES]
        SET [variance_type] = CASE WHEN @var_amount < 0 THEN 'Under' ELSE 'Over' END
        WHERE [id] = @var_id

        FETCH NEXT FROM amount_cursor INTO @var_id, @var_amount
    
    END
    CLOSE amount_cursor
    DEALLOCATE amount_cursor

    --Update the last_updated_by, last_update_dt columns
    UPDATE [dbo].[LTR_CASHIER_VARIANCES]
    SET    [last_updated_by] = dbo.FS_USER(), [last_update_dt] = getdate()
    FROM   [dbo].[LTR_CASHIER_VARIANCES] a, inserted b
    WHERE  a.id = b.id

END
GO
