/*
--DROP TABLE [dbo].[LT_HISTORY_TICKET]
--GO

CREATE TABLE [dbo].[LT_HISTORY_TICKET] ([history_no] int IDENTITY (1,1), [attendance_type] varchar(50), [performance_type] varchar(50), [order_no] int, [sli_no] int, [li_seq_no] int, [title_name] varchar(30), [perf_no] int,
                                        [perf_date] char(10), [perf_time] char(8), [zone_no] int, [production_name] varchar(50), [production_name_short] varchar(50), [production_name_long] varchar(150), 
                                        [comp_code] int, [comp_code_name] varchar(50), [order_payment_status] varchar(50), [price_type] int, [price_type_name] varchar(50), [due_amt] decimal(18,2), 
                                        [paid_amt] decimal(18,2), [sale_total] int, [performance_date] char(10), [performance_time] char(8), [scan_admission_date] char(10), [scan_admission_time] char(8), 
                                        [scan_device] varchar(30), [scan_admission_adult] int, [scan_admission_child] int, [scan_admission_other] int, [scan_admission_auto] int, [scan_admission_total] int, 
                                        [sli_status] int, [customer_no] int, [create_dt] datetime)
GO

GRANT SELECT ON [dbo].[LT_HISTORY_TICKET] TO impusers
GO

CREATE CLUSTERED INDEX [ix_LT_HISTORY_TICKET_perf_date] ON [dbo].[LT_HISTORY_TICKET] ([perf_date] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_performance_type] ON [dbo].[LT_HISTORY_TICKET] ([performance_type] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_price_type_name] ON [dbo].[LT_HISTORY_TICKET] ([price_type_name] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_customer_no] ON [dbo].[LT_HISTORY_TICKET] ([customer_no] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
*/


--DROP TABLE [dbo].[LT_HISTORY_TICKET_ARCHIVE]
--GO

/*
CREATE TABLE [dbo].[LT_HISTORY_TICKET_ARCHIVE] ([history_no] int IDENTITY (1,1), [attendance_type] varchar(50), [performance_type] varchar(50), [order_no] int, [sli_no] int, [li_seq_no] int, [title_name] varchar(30), [perf_no] int,
                                        [perf_date] char(10), [perf_time] char(8), [zone_no] int, [production_name] varchar(50), [production_name_short] varchar(50), [production_name_long] varchar(150), 
                                        [comp_code] int, [comp_code_name] varchar(50), [order_payment_status] varchar(50), [price_type] int, [price_type_name] varchar(50), [due_amt] decimal(18,2), 
                                        [paid_amt] decimal(18,2), [sale_total] int, [performance_date] char(10), [performance_time] char(8), [scan_admission_date] char(10), [scan_admission_time] char(8), 
                                        [scan_device] varchar(30), [scan_admission_adult] int, [scan_admission_child] int, [scan_admission_other] int, [scan_admission_auto] int, [scan_admission_total] int, 
                                        [sli_status] int, [customer_no] int, [create_dt] DATETIME)
GO

GRANT SELECT ON [dbo].[LT_HISTORY_TICKET] TO impusers
GO

CREATE CLUSTERED INDEX [ix_LT_HISTORY_TICKET_ARCHIVE_perf_date] ON [dbo].[LT_HISTORY_TICKET_ARCHIVE] ([perf_date] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_ARCHIVE_performance_type] ON [dbo].[LT_HISTORY_TICKET_ARCHIVE] ([performance_type] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_ARCHIVE_price_type_name] ON [dbo].[LT_HISTORY_TICKET_ARCHIVE] ([price_type_name] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_LT_HISTORY_TICKET_ARCHIVE_customer_no] ON [dbo].[LT_HISTORY_TICKET_ARCHIVE] ([customer_no] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
*/


/*
DROP TABLE [dbo].[LTR_HISTORY_DATES]

CREATE TABLE [dbo].[LTR_HISTORY_DATES] (id int IDENTITY (1,1), history_type varchar(10), history_dt datetime, create_dt datetime, [created_by] varchar(8), [create_loc] varchar(16), [last_update_dt] datetime, [last_updated_by] varchar(8))

CREATE CLUSTERED INDEX [ix_LTR_HISTORY_DATES_history_dt] ON [dbo].[LTR_HISTORY_DATES] ([history_dt] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_history_type]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_history_type]  DEFAULT ('') FOR [history_type]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_HISTORY_DATES_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_HISTORY_DATES] ADD  CONSTRAINT [DF_LTR_HISTORY_DATES_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_HISTORY_DATES] TO impusers
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_HISTORY_DATES_TU]'))
    DROP TRIGGER [dbo].[LTR_HISTORY_DATES_TU]
GO

CREATE TRIGGER [dbo].[LTR_HISTORY_DATES_TU] ON [dbo].[LTR_HISTORY_DATES] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_HISTORY_DATES]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_HISTORY_DATES] a, inserted b
    WHERE	a.id = b.id
 
END
GO
*/