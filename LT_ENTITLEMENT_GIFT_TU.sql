USE [impresario]
GO

/****** Object:  Trigger [dbo].[LT_ENTITLEMENT_GIFT_TU]    Script Date: 7/9/2019 11:21:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tessitura Network Jon Ballinger
-- Create date: 2/9/2018
-- Description:	[LT_ENTITLEMENT_GIFT_TU] update trigger.  context variables are checked to determine how to process update.  Audit entries are written. 
-- Edited 7-2-2019 ADuffy-Brown to use getdate() for membership entitlements and init_dt for is_ad_hoc = 'Y'
-- =============================================

ALTER TRIGGER [dbo].[LT_ENTITLEMENT_GIFT_TU] ON [dbo].[LT_ENTITLEMENT_GIFT] FOR UPDATE 

AS

SET NOCOUNT ON

/* If we have a gift customer #, 
	AND the gift was created and is NOW being assigned 
	AND there is no existing LTX_CUST_ENTITLEMENT row, create it */


declare @is_merging varchar(500)
select @is_merging=dbo.FS_GET_PARAM_FROM_APPNAME('MERGING')

if @is_merging='Y'
	return
exec ap_set_context 'gift_tg', 'Y'

if exists( select 1 
				from inserted a
				where a.gift_status='C' and a.gift_customer_no is  not null)
		begin
					rollback transaction         
					raiserror ('Gift customer can only be assigned when gift is being accepted.', 16, 1)
					return
		end 


	if exists( select 1 
				from inserted a
				where a.gift_status='A' and a.gift_customer_no is null)
		begin
					rollback transaction         
					raiserror ('Gifts can only be accepted with gift recipient customer number.', 16, 1)
					return
		end 

--select * into eh_inserted from inserted
--select * into eh_deleted from deleted

INSERT dbo.LTX_CUST_ENTITLEMENT (
	customer_no,
	cust_memb_no,
	entitlement_no,
	num_ent,
	gift_no,
	init_dt,
	expr_dt)
SELECT 
	a.gift_customer_no,
	0,
	a.entitlement_no,
	a.num_items,
	a.id_key,
	--a.init_dt, --original
	init_dt = case when e.is_adhoc = 'y' then a.init_dt else getdate() end, --added 7/9/2019 
	a.expr_dt
FROM dbo.LT_ENTITLEMENT_GIFT a
	JOIN inserted b ON a.id_key = b.id_key
	join deleted d on a.id_key=d.id_key
	LEFT JOIN dbo.LTX_CUST_ENTITLEMENT ce ON ce.gift_no = a.id_key
	AND ce.gift_no IS NULL /* ce row does not exist */
	join LTR_ENTITLEMENT e on b.entitlement_no = e.entitlement_no --added 7/9/2019 for is_adhoc value
WHERE d.gift_status = 'C' and b.gift_status='A'
and a.gift_customer_no IS NOT NULL



UPDATE 	a
SET
	last_updated_by = user_name(),
	last_update_dt = getdate()
FROM dbo.LT_ENTITLEMENT_GIFT a
	JOIN inserted b ON a.id_key = b.id_key



exec ap_set_context 'gift_tg', 'N'


GO


