USE [impresario]
GO

/****** Object:  Table [dbo].[LT_GOCITY_SCANS]    Script Date: 1/9/2020 11:34:33 AM ******/
DROP TABLE [dbo].[LT_GOCITY_SCANS]
GO

/****** Object:  Table [dbo].[LT_GOCITY_SCANS]    Script Date: 1/9/2020 11:34:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LT_GOCITY_SCANS](
	[go_id] [INT] DEFAULT 0,
	[file_name] NVARCHAR(100) NULL,
	[go_scan_dt] [DATETIME] NULL,
	[go_confirmation_code] [VARCHAR](50) NULL,
	[go_pass_type] [VARCHAR](50) NULL,
	[go_product_type] [VARCHAR](50) NULL,
	[go_attraction] [VARCHAR](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


