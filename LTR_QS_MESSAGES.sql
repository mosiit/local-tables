USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_QS_MESSAGES]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_QS_Messages](
	    [id] [int] NOT NULL,
    	[description] [varchar](30) NULL,
        [message_layout] int NULL,
        [message_row] int NULL,
        [message_column] int NULL,
        [message_001] varchar(30) NULL,
        [message_002] varchar(30) NULL,
        [message_003] varchar(20) NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_QS_MESSAGES] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_description]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_description]  DEFAULT ('') FOR [description]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_layout]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_layout]  DEFAULT (0) FOR [message_layout]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_row]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_row]  DEFAULT (0) FOR [message_row]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_column]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_column]  DEFAULT (0) FOR [message_column]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_001]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_001]  DEFAULT ('') FOR [message_001]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_002]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_002]  DEFAULT ('') FOR [message_002]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_message_003]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_message_003]  DEFAULT ('') FOR [message_003]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_QS_MESSAGES_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_QS_MESSAGES] ADD  CONSTRAINT [DF_LTR_QS_MESSAGES_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_QS_MESSAGES] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_QS_MESSAGES'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] SET [dddw_table] = 'T_SALES_LAYOUT', [dddw_value] = 'id', [dddw_description] = 'description', [dddw_where] = 'inactive = ''N'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_QS_MESSAGES') and [column_name] = 'message_layout'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_QS_MESSAGES_TU]'))
    DROP TRIGGER [dbo].[LTR_QS_MESSAGES_TU]
GO

CREATE TRIGGER [dbo].[LTR_QS_MESSAGES_TU] ON [dbo].[LTR_QS_MESSAGES] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_QS_MESSAGES]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_QS_MESSAGES] a, inserted b
    WHERE	a.id = b.id
 
END
GO
