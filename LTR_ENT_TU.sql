USE [impresario]
GO

/****** Object:  Trigger [dbo].[LTR_ENT_TU]    Script Date: 6/10/2019 1:16:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER TRIGGER [dbo].[LTR_ENT_TU] ON [dbo].[LTR_ENTITLEMENT] FOR UPDATE 
AS

SET NOCOUNT ON


if exists(select 1 from inserted where is_adhoc='Y' and reset_type is not null)
	begin
        rollback transaction
        raiserror ('Reset type cannot be set when entitlement is adhoc.', 16, 1)
		return
    end

if exists(select 1 from inserted where is_adhoc='N' and reset_type is  null)
	begin
        rollback transaction
        raiserror ('Please set reset type.', 16, 1)
		return
    end


if exists(select 1 from inserted where is_adhoc='Y' and memb_level_no is not null)
	begin
        rollback transaction
        raiserror ('Memb level cannot be set when entitlement is adhoc.', 16, 1)
		return
    end

if exists(select 1 from inserted where is_adhoc='N' and memb_level_no is  null)
	begin
        rollback transaction
        raiserror ('Please set memb level.', 16, 1)
		return
    end



if exists(select 1 from inserted where is_adhoc='N' and [fixed_init_dt] is not null)
	begin
        rollback transaction
        raiserror ('Fixed init date cannot be set when entitlement is not  adhoc.', 16, 1)
		return
    end

if exists(select 1 from inserted where is_adhoc='N' and [fixed_expr_dt] is not null)
	begin
        rollback transaction
        raiserror ('Fixed expr date cannot be set when entitlement is not  adhoc.', 16, 1)
		return
    end

if exists(select 1 from inserted where is_adhoc='N' and [relative_num_days] is not null)
	begin
        rollback transaction
        raiserror ('Relative number of days cannot be set when entitlement is not  adhoc.', 16, 1)
		return
    end


if exists(select 1 from inserted where is_adhoc='Y' and ([fixed_init_dt]  is null and [fixed_expr_dt] is null  and [relative_num_days] is null) ) 
	begin
        rollback transaction
        raiserror ('Please set the init and expr dates or set realtive date.', 16, 1)
		return
    end


if exists(select 1 from inserted where is_adhoc='Y' and (
		([fixed_init_dt]  is not null or  [fixed_expr_dt] is not null)   and [relative_num_days] is not null) ) 
	begin
        rollback transaction
        raiserror ('Cannot have fixed dates and relative number days set at same time.', 16, 1)
		return
    end


if exists(select 1 from inserted where is_adhoc='Y' and (
		([fixed_init_dt]  is not null and  [fixed_expr_dt] is  null)   and [relative_num_days] is null) ) 
	begin
        rollback transaction
        raiserror ('Please set fixed expr date', 16, 1)
		return
    end
	
if exists(select 1 from inserted where is_adhoc='Y' and (
		([fixed_init_dt]  is  null and  [fixed_expr_dt] is  not null)   and [relative_num_days] is null) ) 
	begin
        rollback transaction
        raiserror ('Please set fixed init date', 16, 1)
		return
    end


if exists(select 1 from inserted where coalesce(max_num_ent,0) =0)
begin
	UPDATE 	a
	SET
		a.max_num_ent = a.num_ent
	FROM dbo.LTR_ENTITLEMENT a
		JOIN inserted b ON a.entitlement_no = b.entitlement_no
end 




UPDATE 	a
SET
	last_updated_by = user_name(),
	last_update_dt = getdate()
FROM dbo.LTR_ENTITLEMENT a
	JOIN inserted b ON a.entitlement_no = b.entitlement_no



GO

