USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--DROP TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI](
	    [id] [int] NOT NULL,
        [performance_time] [VARCHAR](8) NOT NULL,
        [public_production] [int] NOT NULL,
        [performance_start_dt] DATETIME NULL,
        [performance_end_dt] DATETIME NULL,
        [notes] [VARCHAR] (255) NOT NULL,
        [inactive] [char](1) NOT NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_SCHOOL_TO_PUBLIC_OMNI] PRIMARY KEY NONCLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'ix_LTR_SCHOOL_TO_PUBLIC_OMNI_date_range')
    DROP INDEX [ix_LTR_SCHOOL_TO_PUBLIC_OMNI_date_range] ON [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]
GO

CREATE CLUSTERED INDEX [ix_LTR_SCHOOL_TO_PUBLIC_OMNI_date_range] ON [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ([performance_start_dt] ASC, [performance_end_dt] ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_performance_time]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_perforance_time]  DEFAULT ('') FOR [performance_time]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_public_production]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_public_production]  DEFAULT (0) FOR [public_production]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_notes]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_notes]  DEFAULT ('') FOR [notes]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_SCHOOL_TO_PUBLIC_OMNI_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_SCHOOL_TO_PUBLIC_OMNI_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_SCHOOL_TO_PUBLIC_OMNI_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_SCHOOL_TO_PUBLIC_OMNI_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_SCHOOL_TO_PUBLIC_OMNI_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] ADD  CONSTRAINT [DF_LTR_SCHOOL_TO_PUBLIC_OMNI_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_SCHOOL_TO_PUBLIC_OMNI'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [dddw_table] = 'LV_PRODUCTION_ELEMENTS_PRODUCTION', [dddw_value] = 'production_no', [dddw_description] = 'production_name',
    [dddw_where] = 'title_no = 161 and production_name NOT LIKE ''SCH%'''
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_SCHOOL_TO_PUBLIC_OMNI') and [column_name] = 'public_production'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_TU]'))
    DROP TRIGGER [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_TU]
GO

CREATE TRIGGER [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI_TU] ON [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] a, inserted b
    WHERE	a.id = b.id
 
END
GO

--DELETE FROM [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI]
--INSERT INTO [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] (id, performance_time, public_production,performance_start_dt, performance_end_dt, notes) VALUES (1, '11:00', 1392, '5-18-2016', '1-5-2017', 'Test Record')
--INSERT INTO [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] (id, performance_time, public_production,performance_start_dt, performance_end_dt, notes) VALUES (2, '11:00', 1210, '1-20-2016', '1-31-2017', 'Test Record')
--INSERT INTO [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI] (id, performance_time, public_production,performance_start_dt, performance_end_dt, notes) VALUES (3, '11:00', 11943, '1-6-2016', '1-31-2017', 'Test Record (Duplicate)')
--SELECT production_no, production_name FROM dbo.LV_PRODUCTION_ELEMENTS_PRODUCTION WHERE production_no IN (SELECT public_production FROM [dbo].[LTR_SCHOOL_TO_PUBLIC_OMNI])
--SELECT production_no, production_name FROM dbo.LV_PRODUCTION_ELEMENTS_PRODUCTION WHERE production_name = 'Extreme Weather'
--SELECT * FROM dbo.LTR_SCHOOL_TO_PUBLIC_OMNI


        