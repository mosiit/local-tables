USE [impresario]
GO

/****** Object:  Table [dbo].[LTR_CASHIER_VARIANCES]    Script Date: 4/15/2016 3:13:00 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--DROP TABLE [dbo].[LTR_CASHIER_VARIANCE_REASONS]
--GO

CREATE TABLE [dbo].[LTR_CASHIER_VARIANCE_REASONS](
	[id] [int] IDENTITY(1,1) NOT NULL,
    [description] [varchar] (30) NOT NULL DEFAULT (''),
    [create_dt] [datetime] NULL DEFAULT (GETDATE()),
    [created_by] [varchar] (10) NULL DEFAULT ([dbo].[fs_user]()),
    [last_update_dt] [datetime] NULL DEFAULT (GETDATE()),
    [last_updated_by] [varchar] (10) NULL DEFAULT ([dbo].[fs_user]()),
    [create_loc] [varchar] (16) NULL DEFAULT (HOST_NAME()),
    [inactive] [char] (1) NOT NULL DEFAULT ('N'),
    [control_group] [INT] NOT NULL DEFAULT (-1)
CONSTRAINT [PK_LTR_CASHIER_VARIANCE_REASONS] PRIMARY KEY NONCLUSTERED 
([id] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_CASHIER_VARIANCE_REASONS] TO [ImpUsers]
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_CASHIER_VARIANCE_REASONS'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHIER_VARIANCE_REASONS_TU]'))
    DROP TRIGGER [dbo].[LTR_CASHIER_VARIANCE_REASONS_TU]
GO

CREATE TRIGGER [dbo].[LTR_CASHIER_VARIANCE_REASONS_TU] ON [dbo].[LTR_CASHIER_VARIANCE_REASONS] FOR UPDATE
AS BEGIN

    SET NOCOUNT ON 

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_CASHIER_VARIANCE_REASONS]
    SET 	[last_updated_by] = dbo.FS_USER(), [last_update_dt] = getdate()
    FROM	[dbo].[LTR_CASHIER_VARIANCE_REASONS] a, inserted b
    WHERE	a.id = b.id
 
END
GO

INSERT INTO [dbo].[LTR_CASHIER_VARIANCE_REASONS] ([description]) VALUES ('Unknown')
INSERT INTO [dbo].[LTR_CASHIER_VARIANCE_REASONS] ([description]) VALUES ('Operator Error')
INSERT INTO [dbo].[LTR_CASHIER_VARIANCE_REASONS] ([description]) VALUES ('System Error')
GO
