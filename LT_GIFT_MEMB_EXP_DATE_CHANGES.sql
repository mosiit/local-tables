USE [impresario]
GO

DROP TABLE [dbo].[LT_GIFT_MEMB_EXP_DATE_CHANGES]
GO

/****** Object:  Table [dbo].[LT_GIFT_MEMB_EXP_DATE_CHANGES]    Script Date: 11/22/2016 2:30:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[LT_GIFT_MEMB_EXP_DATE_CHANGES](
	[cust_memb_no] [int] NOT NULL,
	[customer_no] [int] NULL,
	[campaign_no] [int] NULL,
	[memb_org_no] [int] NULL,
	[memb_level] [varchar](3) NULL,
	[memb_amt] [money] NULL,
	[ben_provider] [int] NULL,
	[init_dt] [datetime] NULL,
	[expr_dt] [datetime] NULL,
	[current_status] [int] NULL,
	[NRR_status] [char](2) NULL,
	[declined_ind] [char](1) NULL,
	[AVC_amt] [money] NULL,
	[orig_expiry_dt] [datetime] NULL,
	[orig_memb_level] [varchar](10) NULL,
	[cur_record] [char](1) NULL,
	[ben_holder_ind] [char](1) NULL,
	[recog_amt] [money] NULL,
	[category_trend] [int] NULL,
	[create_loc] [varchar](16) NULL CONSTRAINT [DF_LT_GIFT_MEMB_EXP_DATE_CHANGES_create_loc]  DEFAULT ([dbo].[fs_location]()),
	[created_by] [char](8) NULL CONSTRAINT [DF_LT_GIFT_MEMB_EXP_DATE_CHANGES_created_by]  DEFAULT ([dbo].[fs_user]()),
	[create_dt] [datetime] NULL CONSTRAINT [LT_GIFT_MEMB_EXP_DATE_CHANGES_DF_create_d]  DEFAULT (getdate()),
	[last_updated_by] [char](8) NULL CONSTRAINT [DF_LT_GIFT_MEMB_EXP_DATE_CHANGES_last_updated_by]  DEFAULT ([dbo].[fs_user]()),
	[last_update_dt] [datetime] NOT NULL CONSTRAINT [DF_LT_GIFT_MEMB_EXP_DATE_CHANGES_last_update_dt]  DEFAULT (getdate())
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[LT_GIFT_MEMB_EXP_DATE_CHANGES] ADD [notes] [varchar](1024) NULL
 CONSTRAINT [PK_LT_GIFT_MEMB_EXP_DATE_CHANGES_cust_memb_no] PRIMARY KEY NONCLUSTERED 
(
	[cust_memb_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Index [IND_LT_GIFT_MEMB_EXP_DATE_CHANGES_cust_no]    Script Date: 11/22/2016 2:30:59 PM ******/
CREATE CLUSTERED INDEX [IND_LT_GIFT_MEMB_EXP_DATE_CHANGES_cust_no] ON [dbo].[LT_GIFT_MEMB_EXP_DATE_CHANGES]
(
	[customer_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
GO

