USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_PROCEDURE_EXEC_LOG]') AND type in (N'U'))

--    DELETE FROM [dbo].[TR_REFERENCE_COLUMN] WHERE [reference_table_id] = (SELECT id FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PROCEDURE_EXEC_LOG')
--    DELETE FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PROCEDURE_EXEC_LOG'

--    DROP TABLE [dbo].[LTR_PROCEDURE_EXEC_LOG]
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_PROCEDURE_EXEC_LOG]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_PROCEDURE_EXEC_LOG](
	    [id] [int] NOT NULL IDENTITY (1, 1),
        [procedure_name] VARCHAR(100) NOT NULL DEFAULT (''),
        [parameter_str] VARCHAR(255) NOT NULL DEFAULT (''),
        [execute_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
    	[created_by] [varchar](8) NOT NULL DEFAULT ([dbo].[FS_USERNAME]()),
	    [create_dt] [datetime] NOT NULL DEFAULT(GETDATE()),
    	[create_loc] [varchar](16) NOT NULL DEFAULT ([dbo].[FS_LOCATION]()),
	    [last_updated_by] [varchar](8) NOT NULL DEFAULT ([dbo].[FS_USERNAME]()),
    	[last_update_dt] [datetime] NOT NULL DEFAULT (GETDATE()),
        CONSTRAINT [PK_PROCEDURE_EXEC_LOG] PRIMARY KEY NONCLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'ix_LTR_PROCEDURE_EXEC_LOG_procedure_name')
    DROP INDEX [ix_PROCEDURE_EXEC_LOG_procedure_name] ON [dbo].[LTR_PROCEDURE_EXEC_LOG]
GO

CREATE UNIQUE CLUSTERED INDEX [ix_PROCEDURE_EXEC_LOG_procedure_name] ON [dbo].[LTR_PROCEDURE_EXEC_LOG] ([procedure_name] ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_PROCEDURE_EXEC_LOG] TO [impusers], [tessitura_app]
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_PROCEDURE_EXEC_LOG'
GO

UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [column_type] = 'display'
WHERE [reference_table_id] = (SELECT id FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_PROCEDURE_EXEC_LOG') AND [column_type] = 'editable'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_PROCEDURE_EXEC_LOG_TU]'))
    DROP TRIGGER [dbo].[LTR_PROCEDURE_EXEC_LOG_TU]
GO

CREATE TRIGGER [dbo].[LTR_PROCEDURE_EXEC_LOG_TU] ON [dbo].[LTR_PROCEDURE_EXEC_LOG] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_PROCEDURE_EXEC_LOG]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_PROCEDURE_EXEC_LOG] a, inserted b
    WHERE	a.id = b.id
 
END
GO


SELECT * FROM [dbo].[LTR_PROCEDURE_EXEC_LOG] ORDER BY [procedure_name]

