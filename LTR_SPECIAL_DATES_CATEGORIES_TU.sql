USE [impresario]
GO

/****** Object:  Trigger [LTR_SPECIAL_DATES_CATEGORIES_TU]    Script Date: 11/22/2019 1:19:53 PM ******/
DROP TRIGGER [dbo].[LTR_SPECIAL_DATES_CATEGORIES_TU]
GO

/****** Object:  Trigger [dbo].[LTR_SPECIAL_DATES_CATEGORIES_TU]    Script Date: 11/22/2019 1:19:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[LTR_SPECIAL_DATES_CATEGORIES_TU] ON [dbo].[LTR_SPECIAL_DATES_CATEGORIES] FOR UPDATE
AS BEGIN

 SET NOCOUNT ON 

 --Update the last_updated_by, last_update_dt columns
 UPDATE 	[dbo].[LTR_SPECIAL_DATES_CATEGORIES]
 SET 	last_updated_by = [dbo].FS_USERNAME(), last_update_dt = GETDATE()
 FROM	[dbo].[LTR_SPECIAL_DATES_CATEGORIES] a, inserted b
 WHERE	a.[Category] = b.[Category]
 
END

GO


