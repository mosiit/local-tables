USE [impresario]
GO

/****** Object:  Table [dbo].[LT_CASH_OUT_DATA]    Script Date: 4/15/2016 3:13:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[LT_CASH_OUT_DATA]
GO

CREATE TABLE [dbo].[LT_CASH_OUT_DATA](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[payment_operator_location] [varchar](30) NOT NULL,
	[payment_operator] [varchar](8) NOT NULL,
	[payment_operator_first_name] [varchar](30) NOT NULL,
	[payment_operator_last_name] [varchar](30) NOT NULL,
	[payment_date] [varchar](10) NOT NULL,
	[payment_type_name] [varchar](30) NOT NULL,
	[payment_amount] money NOT NULL,
	[inactive] char(1) NULL,
	[created_by] varchar(8) NULL,
	[created_dt] [datetime] NULL,
	[created_loc] varchar(16) NULL,
	[last_updated_by] varchar(8) NULL,
	[last_updated_dt] [datetime] NULL,
 CONSTRAINT [PK_LT_CASH_OUT_DATA] PRIMARY KEY CLUSTERED 
(
	[payment_operator_location] ASC,
	[payment_operator] ASC,
	[payment_date] ASC,
	[payment_type_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_inactive]  DEFAULT ('N') FOR [inactive]
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_create_dt]  DEFAULT (getdate()) FOR [created_dt]
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_create_loc]  DEFAULT (host_name()) FOR [created_loc]
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
GO

ALTER TABLE [dbo].[LT_CASH_OUT_DATA] ADD  CONSTRAINT [DF_LT_CASH_OUT_DATA_last_update_dt]  DEFAULT (getdate()) FOR [last_updated_dt]
GO

GRANT SELECT ON [dbo].[LT_CASH_OUT_DATA] TO [ImpUsers]
GO