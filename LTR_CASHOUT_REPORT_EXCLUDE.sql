USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

--DROP TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE]
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE](
	    [id] [int] NOT NULL,
        [code_type] [varchar] (30) NOT NULL,
        [code_value] [varchar] (30) NOT NULL,
        [code_value_short] [varchar] (20) NOT NULL,
        [code_notes] [VARCHAR] (100) NOT NULL,
        [inactive] [char](1) NOT NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
        CONSTRAINT [PK_LTR_CASHOUT_REPORT_EXCLUDE] PRIMARY KEY NONCLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'uq_LTR_CASHOUT_REPORT_EXCLUDE_machine_name')
    DROP INDEX [ug_LTR_CASHOUT_REPORT_EXCLUDE_code_type_code_value] ON [dbo].[LTR_CASHOUT_REPORT_EXCLUDE]
GO

CREATE UNIQUE CLUSTERED INDEX [uq_LTR_CASHOUT_REPORT_EXCLUDE_code_type_code_value] ON [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ([code_type] ASC, code_value ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_code_type]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_code_type]  DEFAULT ('') FOR [code_type]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE_code_value]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_code_value]  DEFAULT ('') FOR [code_value]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE_code_value_short]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_code_value_short]  DEFAULT ('') FOR [code_value_short]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE_code_notes]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_code_notes]  DEFAULT ('') FOR [code_notes]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CASHOUT_REPORT_EXCLUDE_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] ADD  CONSTRAINT [DF_LTR_CASHOUT_REPORT_EXCLUDE_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_CASHOUT_REPORT_EXCLUDE'
GO


UPDATE [dbo].[TR_REFERENCE_COLUMN] 
SET [dddw_table] = 'TR_GOOESOFT_DROPDOWN', [dddw_value] = 'description', [dddw_description] = 'description',
    [dddw_where] = 'code = 1018'
WHERE [reference_table_id] = (SELECT [id] FROM [dbo].[TR_REFERENCE_TABLE] WHERE [table_name] = 'LTR_CASHOUT_REPORT_EXCLUDE') and [column_name] = 'code_type'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CASHOUT_REPORT_EXCLUDE_TU]'))
    DROP TRIGGER [dbo].[LTR_CASHOUT_REPORT_EXCLUDE_TU]
GO

CREATE TRIGGER [dbo].[LTR_CASHOUT_REPORT_EXCLUDE_TU] ON [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_CASHOUT_REPORT_EXCLUDE]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_CASHOUT_REPORT_EXCLUDE] a, inserted b
    WHERE	a.id = b.id
 
END
GO


INSERT INTO dbo.LTR_CASHOUT_REPORT_EXCLUDE ([id], [code_type], [code_value], [code_value_short], [code_notes])
VALUES  (1, 'Price Type Reason', '1 Step Renwl - CC fail <30', 'OSF', ''),
        (2, 'Price Type Reason', '1 Step Renwl - CC fail 31+', 'OSE', ''),
        (3, 'Price Type Reason', '1 Step Renwl - Update Card', 'OSC', ''),
        (4, 'Price Type Reason', '1 Step Renwl with Upgrade', 'OSU', ''),
        (5, 'Price Type Reason', 'Beverage', 'BEV', ''),
        (6, 'Price Type Reason', 'Grilled Veggie', 'VEG', ''),
        (7, 'Price Type Reason', 'Ham and Cheese', 'HAM', ''),
        (8, 'Price Type Reason', 'Member payment error/write off', 'WO', ''),
        (9, 'Price Type Reason', 'Minimum Event Fee', 'MIN', ''),
        (10, 'Price Type Reason', 'Package Fee', 'FEE', ''),
        (11, 'Price Type Reason', 'Pizza Party', 'PIZ', ''),
        (12, 'Price Type Reason', 'Retirement per HR', 'HR', ''),
        (13, 'Price Type Reason', 'Snack Pack', 'SNA', ''),
        (14, 'Price Type Reason', 'Staff/vol membership', 'SVM', ''),
        (15, 'Price Type Reason', 'Turkey and Gouda', 'TUR', '')
        
SELECT * FROM dbo.LTR_CASHOUT_REPORT_EXCLUDE


        